package sample.tasks;

import javafx.application.Platform;
import javafx.concurrent.Task;
import sample.Controller;
import sample.simulation.Space;
import sample.simulation.grains.Field;

public class GenerateSpaceTask extends Task<Space> {

    private int boardX;
    private int boardY;
    private Controller controller;
    Space space;

    public GenerateSpaceTask(int boardX, int boardY, Controller controller) {
        this.boardX = boardX;
        this.boardY = boardY;
        this.controller = controller;
    }

    @Override
    public Space call() {
        space = new Space(boardX, boardY, controller.boundaryConditions);
        updateProgress(0, space.x);
        space.fields = new Field[space.x][space.y];
        for (int i = 0; i < space.x; i++) {
            for (int j = 0; j < space.y; j++) {
                space.fields[i][j] = new Field(i, j, null);
            }
            updateProgress(i, space.x);
        }
        updateProgress(space.x, space.x);


        return space;
    }

    @Override
    protected void succeeded() {
        Platform.runLater(() -> {
            synchronized (controller) {
                controller.updateOutput(space.generate(), space, true);
            }
        });


        super.succeeded();
        updateMessage("Task: generated Space!");
    }

    @Override
    protected void cancelled() {
        super.cancelled();
        updateMessage("Task: Generating space was canceled!");
    }

    @Override
    protected void failed() {
        super.failed();
        updateMessage("Task: Generating space failed!");
    }

}
