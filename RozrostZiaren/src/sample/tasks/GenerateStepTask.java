package sample.tasks;

import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.paint.Color;
import sample.Controller;
import sample.simulation.NeighbourhoodType;
import sample.simulation.Space;
import sample.simulation.grains.Field;
import sample.simulation.grains.Grain;
import sample.simulation.neighbors.*;

import java.util.ArrayList;
import java.util.List;


public class GenerateStepTask extends Task<Space> {

    private Space space;
    private Controller controller;
    private int steps;
    private NeighbourhoodType neighbourhoodType;
    private int phase;

    public GenerateStepTask(Space space, Controller controller, int steps, NeighbourhoodType neighbourhoodType) {
        this.controller = controller;
        this.space = space;
        this.steps = steps;
        this.neighbourhoodType = neighbourhoodType;
        this.phase = controller.getPhase();
    }


    @Override
    public Space call() throws Exception {
        for (int k = 0; k < steps; k++) {
            updateProgress(k * space.x, steps * space.x);
            List<Field> toUpdate = new ArrayList<>();
            Space newSpace = (Space) space.clone();

            if (space.isPlace()) {
                Neighbourhood neighbourhood;
                switch (neighbourhoodType) {
                    case VonNeumann:
                        neighbourhood = new VonNeumannNeighbourhood(neighbourhoodType, space.boundaryConditions);
                        break;
                    case Moore:
                        neighbourhood = new MooreNeighbourhood(neighbourhoodType, space.boundaryConditions);
                        break;
                    case HexagonalLeft:
                        neighbourhood = new HexagonalLeftNeighbourhood(neighbourhoodType, space.boundaryConditions);
                        break;
                    case HexagonalRight:
                        neighbourhood = new HexagonalRightNeighbourhood(neighbourhoodType, space.boundaryConditions);
                        break;
                    case HexagonalRandom:
                        neighbourhood = new HexagonalRandomNeighbourhood(neighbourhoodType, space.boundaryConditions);
                        break;
                    case PentagonalRight:
                        neighbourhood = new PentagonalRightNeighbourhood(neighbourhoodType, space.boundaryConditions);
                        break;
                    case PentagonalDown:
                        neighbourhood = new PentagonalDownNeighbourhood(neighbourhoodType, space.boundaryConditions);
                        break;
                    case PentagonalLeft:
                        neighbourhood = new PentagonalLeftNeighbourhood(neighbourhoodType, space.boundaryConditions);
                        break;
                    case PentagonalTop:
                        neighbourhood = new PentagonalTopNeighbourhood(neighbourhoodType, space.boundaryConditions);
                        break;
                    case PentagonalRandom:
                        neighbourhood = new PentagonalRandomNeighbourhood(neighbourhoodType, space.boundaryConditions);
                        break;
                    case GrainBoundaryCurvature:
                        neighbourhood = new GrainBoundaryCurvatureNeighbourhood(neighbourhoodType, space.boundaryConditions, controller.grainThreshold);
                        break;
                    default:
                        neighbourhood = new VonNeumannNeighbourhood(neighbourhoodType, space.boundaryConditions);
                        break;

                }

                for (int i = 0; i < space.x; i++) {
                    for (int j = 0; j < space.y; j++) {
                        if (space.fields[i][j].isEmpty()) {

                            Color newColor;
                            newColor = neighbourhood.fieldColorCalc(space, i, j);
                            if (newColor != null) {

                                for (Grain grain : newSpace.grains) {
                                    if (grain.getColor().equals(newColor)) {
                                        if (grain.getPhase() == controller.getPhase()) {
                                            newSpace.fields[i][j].setParent(grain);
                                            grain.add(newSpace.fields[i][j]);
                                        }
                                    }
                                }
                                toUpdate.add(newSpace.fields[i][j]);

                            }
                        }
                        updateProgress(k * 2 * space.x + i, steps * 2 * space.x);
                    }
                    updateProgress(k * 2 * space.x + space.x, steps * 2 * space.x);
                }
            } else {
                System.out.println("No more place!");
            }


            updateProgress(k * 2 * space.x + 1.5 * space.x, steps * 2 * space.x);

            newSpace.grains = space.grains;
            newSpace.updateImage(toUpdate);
            space = newSpace;

            updateProgress(k * 2 * space.x + 2 * space.x, steps * 2 * space.x);

            System.out.println(k + " step finished: " + toUpdate.size() + " fields changed");

        }


        Thread.currentThread().interrupt();
        return space;
    }

    @Override
    protected void succeeded() {
        Platform.runLater(() -> {
            synchronized (controller) {
                space.synchoGrains();
                controller.unlockGUI();
                controller.updateOutput(space.returnImagePane(), space, true);
            }
        });


        super.succeeded();
        updateMessage("Task: generated Space!");
    }

    @Override
    protected void cancelled() {
        super.cancelled();
        updateMessage("Task: Generating space was canceled!");
    }

    @Override
    protected void failed() {
        super.failed();
        updateMessage("Task: Generating space failed!");
    }
}