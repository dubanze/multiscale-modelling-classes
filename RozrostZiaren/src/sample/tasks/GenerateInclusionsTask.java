package sample.tasks;

import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.paint.Color;
import sample.Controller;
import sample.simulation.Space;
import sample.simulation.grains.Field;
import sample.simulation.grains.InclusionGrain;
import sample.simulation.grains.MaterialType;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GenerateInclusionsTask extends Task<Space> {
    private int inclusionsAmount;
    private double maxRadius, minRadius;
    private Space space;
    private Controller controller;

    public GenerateInclusionsTask(Space space, Controller controller, int inclusionsAmount, double minRadius, double maxRadius) {
        this.space = space;
        this.controller = controller;
        this.inclusionsAmount = inclusionsAmount;
        this.minRadius = minRadius;
        this.maxRadius = maxRadius;
    }

    @Override
    public synchronized Space call() {

        ArrayList<InclusionGrain> generatedInclusions = new ArrayList<>();
        List<Field> toUpdate = new ArrayList<>();

        Random random = new Random();
        updateProgress(0, inclusionsAmount);

        for (int i = 0; i < inclusionsAmount; i++) {
            ///*
            double x = -1;
            double y = -1;
            boolean againFlag = true;
            while (x < 0 || x > space.x || y < 0 || y > space.y || againFlag) {
                x = random.nextDouble() * space.x;
                y = random.nextDouble() * space.y;

                againFlag = false;
                for (InclusionGrain g : generatedInclusions) {
                    if ((int) g.getX() == (int) x && (int) g.getY() == (int) y) {
                        againFlag = true;
                    }
                }
            }
            double r = -1;
            while (r < 0 || r < minRadius || r > maxRadius) {
                r = minRadius + (maxRadius - minRadius) * random.nextDouble();
            }
            InclusionGrain g = new InclusionGrain(MaterialType.OXYGEN, 1, x, y, r);
            g.setColor(Color.BLACK);
            generatedInclusions.add(g);
            updateProgress(i, inclusionsAmount + space.x);

        }

        updateProgress(inclusionsAmount, inclusionsAmount + space.x);
        for (int p = 0; p < space.x; p++) {

            for (int q = 0; q < space.y; q++) {

                for (InclusionGrain g : generatedInclusions) {
                    double value = ((p - g.getX()) * ((p - g.getX())) + (q - g.getY()) * ((q - g.getY())));
                    double square = g.getRadius() * g.getRadius();
                    if (value < square) {
                        space.fields[p][q].setParent(g);
                        g.add(space.fields[p][q]);
                        space.grains.add(g);
                        toUpdate.add(space.fields[p][q]);

                    }
                }
            }
            updateProgress(inclusionsAmount + p, inclusionsAmount + space.x);
        }
        updateProgress(inclusionsAmount + space.x, inclusionsAmount + space.x);
        space.updateImage(toUpdate);
        //updateProgress(inclusionsAmount, inclusionsAmount);
        return space;
    }

    @Override
    protected void succeeded() {
        Platform.runLater(() -> {

            synchronized (controller) {
                controller.updateOutput(space.generate(), space, true);
            }
        });


        super.succeeded();
        updateMessage("Task: generated Space!");
    }

    @Override
    protected void cancelled() {
        super.cancelled();
        updateMessage("Task: Generating space was canceled!");
    }

    @Override
    protected void failed() {
        super.failed();
        updateMessage("Task: Generating space failed!");
    }

}
