package sample.tasks;

import javafx.application.Platform;
import javafx.concurrent.Task;
import sample.Controller;
import sample.simulation.Space;
import sample.simulation.grains.Field;
import sample.simulation.grains.Grain;
import sample.simulation.grains.MaterialType;

import java.util.Random;

public class GenerateSeedsTask extends Task<Space> {
    private Controller controller;
    private Space space;
    int howMuch;
    int phase;

    public GenerateSeedsTask(Space space, Controller controller, int howMuch, int phase) {
        this.controller = controller;
        this.space = space;
        this.howMuch = howMuch;
        this.phase = phase;
    }

    @Override
    public synchronized Space call() {

        updateProgress(0, howMuch);
        Random randomizer = new Random();
        int iterator = 0;
        switch (controller.seedingType) {

            case Random:
                iterator = 0;
                while (iterator < howMuch) {
                    Grain grain;


                    switch (phase) {
                        case 1:
                            grain = new Grain(MaterialType.FERRITE, this.phase);
                            break;
                        case 2:
                            grain = new Grain(MaterialType.FERRITE, this.phase);
                            break;
                        default:
                            grain = new Grain(MaterialType.IRON, this.phase);
                            break;
                    }
                    grain.setColor(space.nextPaint());
                    boolean added = false;
                    int xr = -1;
                    int yr = -1;
                    while (!added) {
                        xr = randomizer.nextInt(space.x);
                        yr = randomizer.nextInt(space.y);
                        if (space.fields[xr][yr].getParent() == null) {
                            added = true;
                        }
                    }
                    grain.add(space.fields[xr][yr]);
                    space.add(grain);
                    iterator++;
                    updateProgress(iterator, howMuch);

                }
                updateProgress(howMuch, howMuch);
                break;
            case Evenly:
                double a = Math.ceil(Math.sqrt(howMuch));
                double dx = (space.x - 1) / (2 * a);
                double dy = (space.y - 1) / (2 * a);
                for (int i = 1; i <= a; i++) {
                    for (int j = 1; j <= a; j++) {
                        if (iterator < howMuch) {

                            Grain grain;
                            switch (phase) {
                                case 1:
                                    grain = new Grain(MaterialType.AUSTENITE, this.phase);
                                case 2:
                                    grain = new Grain(MaterialType.FERRITE, this.phase);
                                default:
                                    grain = new Grain(MaterialType.IRON, this.phase);
                            }
                            grain.setColor(space.nextPaint());

                            Field field = new Field((int) (i * 2 * dx - dx / 2), (int) (j * 2 * dy - dy / 2), grain);
                            grain.add(field);
                            iterator++;
                            updateProgress(iterator, howMuch);
                        }
                    }
                }
                updateProgress(howMuch, howMuch);
                break;

        }
        return space;
    }

    @Override
    protected void succeeded() {
        Platform.runLater(() -> {
            synchronized (controller) {
                controller.updateOutput(space.generate(), space, true);
            }
        });


        super.succeeded();
        updateMessage("Task: generated Space!");
    }

    @Override
    protected void cancelled() {
        super.cancelled();
        updateMessage("Task: Generating space was canceled!");
    }

    @Override
    protected void failed() {
        super.failed();
        updateMessage("Task: Generating space failed!");
    }
}
