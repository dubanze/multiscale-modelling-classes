package sample.tasks;

import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.paint.Color;
import sample.Controller;
import sample.simulation.NeighbourhoodType;
import sample.simulation.Space;
import sample.simulation.grains.Field;
import sample.simulation.grains.Grain;
import sample.simulation.neighbors.*;

import java.util.ArrayList;
import java.util.List;

public class GenerateRunTask extends Task<Space> {
    private Space space;
    private Controller controller;
    private NeighbourhoodType neighbourhoodType;
    private int phase;

    public GenerateRunTask(Space space, Controller controller, NeighbourhoodType neighbourhoodType) {
        this.space = space;
        this.controller = controller;
        this.neighbourhoodType = neighbourhoodType;
        this.phase = controller.getPhase();
    }

    @Override
    public Space call() throws Exception {
        updateProgress(0, space.x);
        List<Field> toUpdate = new ArrayList<>();
        Space newSpace = (Space) space.clone();


        if (space.isPlace()) {

            Neighbourhood neighbourhood;
            switch (neighbourhoodType) {
                case VonNeumann:
                    neighbourhood = new VonNeumannNeighbourhood(neighbourhoodType, space.boundaryConditions);
                    break;
                case Moore:
                    neighbourhood = new MooreNeighbourhood(neighbourhoodType, space.boundaryConditions);
                    break;
                case HexagonalLeft:
                    neighbourhood = new HexagonalLeftNeighbourhood(neighbourhoodType, space.boundaryConditions);
                    break;
                case HexagonalRight:
                    neighbourhood = new HexagonalRightNeighbourhood(neighbourhoodType, space.boundaryConditions);
                    break;
                case HexagonalRandom:
                    neighbourhood = new HexagonalRandomNeighbourhood(neighbourhoodType, space.boundaryConditions);
                    break;
                case PentagonalRight:
                    neighbourhood = new PentagonalRightNeighbourhood(neighbourhoodType, space.boundaryConditions);
                    break;
                case PentagonalDown:
                    neighbourhood = new PentagonalDownNeighbourhood(neighbourhoodType, space.boundaryConditions);
                    break;
                case PentagonalLeft:
                    neighbourhood = new PentagonalLeftNeighbourhood(neighbourhoodType, space.boundaryConditions);
                    break;
                case PentagonalTop:
                    neighbourhood = new PentagonalTopNeighbourhood(neighbourhoodType, space.boundaryConditions);
                    break;
                case PentagonalRandom:
                    neighbourhood = new PentagonalRandomNeighbourhood(neighbourhoodType, space.boundaryConditions);
                    break;
                case GrainBoundaryCurvature:
                    System.out.println(controller.grainThreshold);
                    controller.handleThresholdChange();
                    neighbourhood = new GrainBoundaryCurvatureNeighbourhood(neighbourhoodType, space.boundaryConditions, controller.grainThreshold);
                    break;
                default:
                    neighbourhood = new VonNeumannNeighbourhood(neighbourhoodType, space.boundaryConditions);
                    break;

            }

            for (int i = 0; i < space.x; i++) {
                for (int j = 0; j < space.y; j++) {

                    if (space.fields[i][j].isEmpty()) {
                        if (space.fields[i][j].getParent() == null) {

                            Color newColor;
                            newColor = neighbourhood.fieldColorCalc(space, i, j);
                            if (newColor != null) {

                                for (Grain grain : newSpace.grains) {
                                    if (grain.getColor().equals(newColor)) {
                                        if (grain.getPhase() == controller.getPhase()) {
                                            newSpace.fields[i][j].setParent(grain);
                                            grain.add(newSpace.fields[i][j]);
                                        }
                                    }
                                }


                                toUpdate.add(newSpace.fields[i][j]);

                            }

                        }
                    }
                    updateProgress(i, space.x);
                }
                updateProgress(space.x, space.x);


            }
        } else {
            System.out.println("No more place!");
        }


        newSpace.grains = space.grains;
        newSpace.updateImage(toUpdate);
        space = newSpace;

        System.out.println(" Next step finished: " + toUpdate.size() + " fields changed");
        return space;
    }

    @Override
    protected void succeeded() {
        updateMessage("Task: generated run step!");
        Platform.runLater(() -> {

            synchronized (controller) {
                controller.space = this.space;

                controller.updateOutput(space.returnImagePane(), space, false);
                controller.recurrentGrow();
            }
        });
        super.succeeded();
    }

    @Override
    protected void cancelled() {
        updateMessage("Task: Generating space was canceled!");
        super.cancelled();
    }

    @Override
    protected void failed() {
        updateMessage("Task: Generating space failed!");
        super.failed();
    }
}
