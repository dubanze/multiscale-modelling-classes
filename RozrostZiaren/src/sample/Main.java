package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    private Stage primaryStage;
    private Controller controller;

    @Override
    public void start(Stage primaryStage) throws Exception{

        this.primaryStage=primaryStage;
        this.primaryStage.setTitle("Simple Grain Growth");
        initRootLayout();
    }


    public static void main(String[] args) {
        launch(args);
    }

    public void initRootLayout() throws Exception{

        Parent root= FXMLLoader.load(getClass().getResource("GrainGrowth.fxml"));
        primaryStage.setScene(new Scene(root, 1280 , 720));
        primaryStage.show();

    }
}
