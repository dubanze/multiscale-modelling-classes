package sample.simulation;

public enum BoundaryConditions {
    Periodic, Nonperiodic
}
