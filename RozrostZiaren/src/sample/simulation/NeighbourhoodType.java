package sample.simulation;


public enum NeighbourhoodType {
    VonNeumann, Moore, HexagonalRight, HexagonalLeft, HexagonalRandom, PentagonalRandom, PentagonalLeft,
    PentagonalRight, PentagonalTop, PentagonalDown, GrainBoundaryCurvature;
}
