package sample.simulation.neighbors;

import javafx.scene.paint.Color;
import sample.simulation.BoundaryConditions;
import sample.simulation.NeighbourhoodType;
import sample.simulation.Space;
import sample.simulation.grains.Grain;

import java.util.ArrayList;
import java.util.Random;

public class HexagonalRandomNeighbourhood extends HexagonalNeighbourhood {
    public HexagonalRandomNeighbourhood(NeighbourhoodType neighbourhoodType, BoundaryConditions boundaryConditions) {
        super(neighbourhoodType, boundaryConditions);
    }

    @Override
    @Deprecated
    public ArrayList<Color> getNeighborColors(Space space, int i, int j) {
        HexagonalNeighbourhood neighbourhood;
        Random random = new Random();
        boolean doLeft = random.nextBoolean();

        if (doLeft) {
            neighbourhood = new HexagonalLeftNeighbourhood(neighbourhoodType, boundaryConditions);
        } else {
            neighbourhood = new HexagonalRightNeighbourhood(neighbourhoodType, boundaryConditions);
        }
        return neighbourhood.getNeighborColors(space, i, j);
    }

    @Override
    public ArrayList<Grain> getNeighborGrains(Space space, int i, int j, int phase) {
        HexagonalNeighbourhood neighbourhood;
        Random random = new Random();
        boolean doLeft = random.nextBoolean();

        if (doLeft) {
            neighbourhood = new HexagonalLeftNeighbourhood(neighbourhoodType, boundaryConditions);
        } else {
            neighbourhood = new HexagonalRightNeighbourhood(neighbourhoodType, boundaryConditions);
        }
        return neighbourhood.getNeighborGrains(space, i, j, phase);
    }
}
