package sample.simulation.neighbors;

import javafx.scene.paint.Color;
import sample.simulation.BoundaryConditions;
import sample.simulation.NeighbourhoodType;
import sample.simulation.Space;
import sample.simulation.grains.BorderGrain;
import sample.simulation.grains.Grain;
import sample.simulation.grains.VoidGrain;

import java.util.ArrayList;

public class PentagonalTopNeighbourhood extends PentagonalNeighbourhood {
    public PentagonalTopNeighbourhood(NeighbourhoodType neighbourhoodType, BoundaryConditions boundaryConditions) {
        super(neighbourhoodType, boundaryConditions);
    }

    @Override
    @Deprecated
    public ArrayList<Color> getNeighborColors(Space space, int i, int j) {
        ArrayList<Color> colors = new ArrayList<>();

        if (i == 0 || i == space.x - 1 || j == 0 || j == space.y - 1) { //on the border
            if (boundaryConditions == BoundaryConditions.Periodic) {
                if (i == 0) {
                    if (j == 0) { //corner
                        colors.add(space.fields[i + 1][j].getColor());
                        colors.add(space.fields[i + 1][space.y - 1].getColor());
                        colors.add(space.fields[i][space.y - 1].getColor());
                        colors.add(space.fields[space.x - 1][space.y - 1].getColor());
                        colors.add(space.fields[space.x - 1][j].getColor());

                    } else if (j == space.y - 1) { //corner
                        colors.add(space.fields[i + 1][j].getColor());
                        colors.add(space.fields[i + 1][j - 1].getColor());
                        colors.add(space.fields[i][j - 1].getColor());
                        colors.add(space.fields[space.x - 1][j - 1].getColor());
                        colors.add(space.fields[space.x - 1][j].getColor());

                    } else { //just edge
                        colors.add(space.fields[i + 1][j].getColor());
                        colors.add(space.fields[i + 1][j - 1].getColor());
                        colors.add(space.fields[i][j - 1].getColor());
                        colors.add(space.fields[space.x - 1][j - 1].getColor());
                        colors.add(space.fields[space.x - 1][j].getColor());

                    }
                } else if (i == space.x - 1) {
                    if (j == 0) { //corner
                        colors.add(space.fields[0][j].getColor());
                        colors.add(space.fields[0][space.y - 1].getColor());
                        colors.add(space.fields[i][space.y - 1].getColor());
                        colors.add(space.fields[i - 1][space.y - 1].getColor());
                        colors.add(space.fields[i - 1][j].getColor());

                    } else if (j == space.y - 1) { //corner
                        colors.add(space.fields[0][j].getColor());
                        colors.add(space.fields[0][j - 1].getColor());
                        colors.add(space.fields[i][j - 1].getColor());
                        colors.add(space.fields[i - 1][j - 1].getColor());
                        colors.add(space.fields[i - 1][j].getColor());

                    } else { //just edge
                        colors.add(space.fields[0][j].getColor());
                        colors.add(space.fields[0][j - 1].getColor());
                        colors.add(space.fields[i][j - 1].getColor());
                        colors.add(space.fields[i - 1][j - 1].getColor());
                        colors.add(space.fields[i - 1][j].getColor());

                    }

                } else if (j == 0) {
                    colors.add(space.fields[i + 1][j].getColor());
                    colors.add(space.fields[i + 1][space.y - 1].getColor());
                    colors.add(space.fields[i][space.y - 1].getColor());
                    colors.add(space.fields[i - 1][space.y - 1].getColor());
                    colors.add(space.fields[i - 1][j].getColor());


                } else if (j == space.y - 1) {
                    colors.add(space.fields[i + 1][j].getColor());
                    colors.add(space.fields[i + 1][j - 1].getColor());
                    colors.add(space.fields[i][j - 1].getColor());
                    colors.add(space.fields[i - 1][j - 1].getColor());
                    colors.add(space.fields[i - 1][j].getColor());

                }

            } else { //non periodic
                if (i == 0) {
                    if (j == 0) { //corner
                        colors.add(space.fields[i + 1][j].getColor());
                        colors.add(Color.BLACK);
                        colors.add(Color.BLACK);
                        colors.add(Color.BLACK);
                        colors.add(Color.BLACK);

                    } else if (j == space.y - 1) { //corner
                        colors.add(space.fields[i + 1][j].getColor());
                        colors.add(space.fields[i + 1][j - 1].getColor());
                        colors.add(space.fields[i][j - 1].getColor());
                        colors.add(Color.BLACK);
                        colors.add(Color.BLACK);

                    } else { //just edge
                        colors.add(space.fields[i + 1][j].getColor());
                        colors.add(space.fields[i + 1][j - 1].getColor());
                        colors.add(space.fields[i][j - 1].getColor());
                        colors.add(Color.BLACK);
                        colors.add(Color.BLACK);

                    }
                } else if (i == space.x - 1) {
                    if (j == 0) { //corner
                        colors.add(Color.BLACK);
                        colors.add(Color.BLACK);
                        colors.add(Color.BLACK);
                        colors.add(Color.BLACK);
                        colors.add(space.fields[i - 1][j].getColor());

                    } else if (j == space.y - 1) { //corner
                        colors.add(Color.BLACK);
                        colors.add(Color.BLACK);
                        colors.add(space.fields[i][j - 1].getColor());
                        colors.add(space.fields[i - 1][j - 1].getColor());
                        colors.add(space.fields[i - 1][j].getColor());

                    } else { //just edge
                        colors.add(Color.BLACK);
                        colors.add(Color.BLACK);
                        colors.add(space.fields[i][j - 1].getColor());
                        colors.add(space.fields[i - 1][j - 1].getColor());
                        colors.add(space.fields[i - 1][j].getColor());

                    }

                } else if (j == 0) {
                    colors.add(space.fields[i + 1][j].getColor());
                    colors.add(Color.BLACK);
                    colors.add(Color.BLACK);
                    colors.add(Color.BLACK);
                    colors.add(space.fields[i - 1][j].getColor());


                } else if (j == space.y - 1) {
                    colors.add(space.fields[i + 1][j].getColor());
                    colors.add(space.fields[i + 1][j - 1].getColor());
                    colors.add(space.fields[i][j - 1].getColor());
                    colors.add(space.fields[i - 1][j - 1].getColor());
                    colors.add(space.fields[i - 1][j].getColor());

                }
            }
        } else { //inside space
            colors.add(space.fields[i + 1][j].getColor());
            colors.add(space.fields[i + 1][j - 1].getColor());
            colors.add(space.fields[i][j - 1].getColor());
            colors.add(space.fields[i - 1][j - 1].getColor());
            colors.add(space.fields[i - 1][j].getColor());

        }

        return colors;
    }

    @Override
    public ArrayList<Grain> getNeighborGrains(Space space, int i, int j, int phase) {


        ArrayList<Grain> grains = new ArrayList<>();

        if (i == 0 || i == space.x - 1 || j == 0 || j == space.y - 1) {
            if (boundaryConditions == BoundaryConditions.Periodic) {
                if (i == 0) {
                    if (j == 0) {
                        if (space.fields[i + 1][j].getPhase() == phase) {
                            grains.add(space.fields[i + 1][j].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[i + 1][space.y - 1].getPhase() == phase) {
                            grains.add(space.fields[i + 1][space.y - 1].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[i][space.y - 1].getPhase() == phase) {
                            grains.add(space.fields[i][space.y - 1].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[space.x - 1][space.y - 1].getPhase() == phase) {
                            grains.add(space.fields[space.x - 1][space.y - 1].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[space.x - 1][j].getPhase() == phase) {
                            grains.add(space.fields[space.x - 1][j].getParent());
                        } else grains.add(new VoidGrain());


                    } else if (j == space.y - 1) { //corner
                        if (space.fields[i + 1][j].getPhase() == phase) {
                            grains.add(space.fields[i + 1][j].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[i + 1][j - 1].getPhase() == phase) {
                            grains.add(space.fields[i + 1][j - 1].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[i][j - 1].getPhase() == phase) {
                            grains.add(space.fields[i][j - 1].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[space.x - 1][j - 1].getPhase() == phase) {
                            grains.add(space.fields[space.x - 1][j - 1].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[space.x - 1][j].getPhase() == phase) {
                            grains.add(space.fields[space.x - 1][j].getParent());
                        } else grains.add(new VoidGrain());


                    } else { //just edge
                        if (space.fields[i + 1][j].getPhase() == phase) {
                            grains.add(space.fields[i + 1][j].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[i + 1][j - 1].getPhase() == phase) {
                            grains.add(space.fields[i + 1][j - 1].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[i][j - 1].getPhase() == phase) {
                            grains.add(space.fields[i][j - 1].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[space.x - 1][j - 1].getPhase() == phase) {
                            grains.add(space.fields[space.x - 1][j - 1].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[space.x - 1][j].getPhase() == phase) {
                            grains.add(space.fields[space.x - 1][j].getParent());
                        } else grains.add(new VoidGrain());


                    }
                } else if (i == space.x - 1) {
                    if (j == 0) { //corner
                        if (space.fields[0][j].getPhase() == phase) {
                            grains.add(space.fields[0][j].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[0][space.y - 1].getPhase() == phase) {
                            grains.add(space.fields[0][space.y - 1].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[i][space.y - 1].getPhase() == phase) {
                            grains.add(space.fields[i][space.y - 1].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[i - 1][space.y - 1].getPhase() == phase) {
                            grains.add(space.fields[i - 1][space.y - 1].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[i - 1][j].getPhase() == phase) {
                            grains.add(space.fields[i - 1][j].getParent());
                        } else grains.add(new VoidGrain());


                    } else if (j == space.y - 1) { //corner
                        if (space.fields[0][j].getPhase() == phase) {
                            grains.add(space.fields[0][j].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[0][j - 1].getPhase() == phase) {
                            grains.add(space.fields[0][j - 1].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[i][j - 1].getPhase() == phase) {
                            grains.add(space.fields[i][j - 1].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[i - 1][j - 1].getPhase() == phase) {
                            grains.add(space.fields[i - 1][j - 1].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[i - 1][j].getPhase() == phase) {
                            grains.add(space.fields[i - 1][j].getParent());
                        } else grains.add(new VoidGrain());


                    } else { //just edge
                        if (space.fields[0][j].getPhase() == phase) {
                            grains.add(space.fields[0][j].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[0][j - 1].getPhase() == phase) {
                            grains.add(space.fields[0][j - 1].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[i][j - 1].getPhase() == phase) {
                            grains.add(space.fields[i][j - 1].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[i - 1][j - 1].getPhase() == phase) {
                            grains.add(space.fields[i - 1][j - 1].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[i - 1][j].getPhase() == phase) {
                            grains.add(space.fields[i - 1][j].getParent());
                        } else grains.add(new VoidGrain());


                    }

                } else if (j == 0) {
                    if (space.fields[i + 1][j].getPhase() == phase) {
                        grains.add(space.fields[i + 1][j].getParent());
                    } else grains.add(new VoidGrain());
                    if (space.fields[i + 1][space.y - 1].getPhase() == phase) {
                        grains.add(space.fields[i + 1][space.y - 1].getParent());
                    } else grains.add(new VoidGrain());
                    if (space.fields[i][space.y - 1].getPhase() == phase) {
                        grains.add(space.fields[i][space.y - 1].getParent());
                    } else grains.add(new VoidGrain());
                    if (space.fields[i - 1][space.y - 1].getPhase() == phase) {
                        grains.add(space.fields[i - 1][space.y - 1].getParent());
                    } else grains.add(new VoidGrain());
                    if (space.fields[i - 1][j].getPhase() == phase) {
                        grains.add(space.fields[i - 1][j].getParent());
                    } else grains.add(new VoidGrain());


                } else if (j == space.y - 1) {
                    if (space.fields[i + 1][j].getPhase() == phase) {
                        grains.add(space.fields[i + 1][j].getParent());
                    } else grains.add(new VoidGrain());
                    if (space.fields[i + 1][j - 1].getPhase() == phase) {
                        grains.add(space.fields[i + 1][j - 1].getParent());
                    } else grains.add(new VoidGrain());
                    if (space.fields[i][j - 1].getPhase() == phase) {
                        grains.add(space.fields[i][j - 1].getParent());
                    } else grains.add(new VoidGrain());
                    if (space.fields[i - 1][j - 1].getPhase() == phase) {
                        grains.add(space.fields[i - 1][j - 1].getParent());
                    } else grains.add(new VoidGrain());
                    if (space.fields[i - 1][j].getPhase() == phase) {
                        grains.add(space.fields[i - 1][j].getParent());
                    } else grains.add(new VoidGrain());


                }

            } else { //non periodic
                if (i == 0) {
                    if (j == 0) { //corner
                        if (space.fields[i + 1][j].getPhase() == phase) {
                            grains.add(space.fields[i + 1][j].getParent());
                        } else grains.add(new VoidGrain());

                        grains.add(new BorderGrain());
                        grains.add(new BorderGrain());
                        grains.add(new BorderGrain());
                        grains.add(new BorderGrain());

                    } else if (j == space.y - 1) { //corner
                        if (space.fields[i + 1][j].getPhase() == phase) {
                            grains.add(space.fields[i + 1][j].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[i + 1][j - 1].getPhase() == phase) {
                            grains.add(space.fields[i + 1][j - 1].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[i][j - 1].getPhase() == phase) {
                            grains.add(space.fields[i][j - 1].getParent());
                        } else grains.add(new VoidGrain());

                        grains.add(new BorderGrain());
                        grains.add(new BorderGrain());

                    } else { //just edge
                        if (space.fields[i + 1][j].getPhase() == phase) {
                            grains.add(space.fields[i + 1][j].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[i + 1][j - 1].getPhase() == phase) {
                            grains.add(space.fields[i + 1][j - 1].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[i][j - 1].getPhase() == phase) {
                            grains.add(space.fields[i][j - 1].getParent());
                        } else grains.add(new VoidGrain());

                        grains.add(new BorderGrain());
                        grains.add(new BorderGrain());

                    }
                } else if (i == space.x - 1) {
                    if (j == 0) { //corner
                        grains.add(new BorderGrain());
                        grains.add(new BorderGrain());
                        grains.add(new BorderGrain());
                        grains.add(new BorderGrain());
                        if (space.fields[i - 1][j].getPhase() == phase) {
                            grains.add(space.fields[i - 1][j].getParent());
                        } else grains.add(new VoidGrain());


                    } else if (j == space.y - 1) { //corner
                        grains.add(new BorderGrain());
                        grains.add(new BorderGrain());
                        if (space.fields[i][j - 1].getPhase() == phase) {
                            grains.add(space.fields[i][j - 1].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[i - 1][j - 1].getPhase() == phase) {
                            grains.add(space.fields[i - 1][j - 1].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[i - 1][j].getPhase() == phase) {
                            grains.add(space.fields[i - 1][j].getParent());
                        } else grains.add(new VoidGrain());


                    } else { //just edge
                        grains.add(new BorderGrain());
                        grains.add(new BorderGrain());
                        if (space.fields[i][j - 1].getPhase() == phase) {
                            grains.add(space.fields[i][j - 1].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[i - 1][j - 1].getPhase() == phase) {
                            grains.add(space.fields[i - 1][j - 1].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[i - 1][j].getPhase() == phase) {
                            grains.add(space.fields[i - 1][j].getParent());
                        } else grains.add(new VoidGrain());


                    }

                } else if (j == 0) {
                    if (space.fields[i + 1][j].getPhase() == phase) {
                        grains.add(space.fields[i + 1][j].getParent());
                    } else grains.add(new VoidGrain());

                    grains.add(new BorderGrain());
                    grains.add(new BorderGrain());
                    grains.add(new BorderGrain());
                    if (space.fields[i - 1][j].getPhase() == phase) {
                        grains.add(space.fields[i - 1][j].getParent());
                    } else grains.add(new VoidGrain());


                } else if (j == space.y - 1) {
                    if (space.fields[i + 1][j].getPhase() == phase) {
                        grains.add(space.fields[i + 1][j].getParent());
                    } else grains.add(new VoidGrain());
                    if (space.fields[i + 1][j - 1].getPhase() == phase) {
                        grains.add(space.fields[i + 1][j - 1].getParent());
                    } else grains.add(new VoidGrain());
                    if (space.fields[i][j - 1].getPhase() == phase) {
                        grains.add(space.fields[i][j - 1].getParent());
                    } else grains.add(new VoidGrain());
                    if (space.fields[i - 1][j - 1].getPhase() == phase) {
                        grains.add(space.fields[i - 1][j - 1].getParent());
                    } else grains.add(new VoidGrain());
                    if (space.fields[i - 1][j].getPhase() == phase) {
                        grains.add(space.fields[i - 1][j].getParent());
                    } else grains.add(new VoidGrain());


                }
            }
        } else { //inside space
            if (space.fields[i + 1][j].getPhase() == phase) {
                grains.add(space.fields[i + 1][j].getParent());
            } else grains.add(new VoidGrain());
            if (space.fields[i + 1][j - 1].getPhase() == phase) {
                grains.add(space.fields[i + 1][j - 1].getParent());
            } else grains.add(new VoidGrain());
            if (space.fields[i][j - 1].getPhase() == phase) {
                grains.add(space.fields[i][j - 1].getParent());
            } else grains.add(new VoidGrain());
            if (space.fields[i - 1][j - 1].getPhase() == phase) {
                grains.add(space.fields[i - 1][j - 1].getParent());
            } else grains.add(new VoidGrain());
            if (space.fields[i - 1][j].getPhase() == phase) {
                grains.add(space.fields[i - 1][j].getParent());
            } else grains.add(new VoidGrain());


        }

        return grains;
    }

}
