package sample.simulation.neighbors;

import javafx.scene.paint.Color;
import sample.MathUtils;
import sample.simulation.BoundaryConditions;
import sample.simulation.NeighbourhoodType;
import sample.simulation.Space;
import sample.simulation.grains.BorderGrain;
import sample.simulation.grains.Grain;
import sample.simulation.grains.VoidGrain;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class VonNeumannNeighbourhood implements Neighbourhood {

    protected NeighbourhoodType neighbourhoodType;
    protected BoundaryConditions boundaryConditions;

    public VonNeumannNeighbourhood(NeighbourhoodType neighbourhoodType, BoundaryConditions boundaryConditions) {
        this.neighbourhoodType = neighbourhoodType;
        this.boundaryConditions = boundaryConditions;
    }

    @Override
    @Deprecated
    public Color fieldColorCalc(Space space, int i, int j) {
        ArrayList<Color> colors = getNeighborColors(space, i, j);
        return checkFieldColor(colors);
    }

    @Override
    @Deprecated
    public Color checkFieldColor(ArrayList<Color> colors) {
        return checkFieldColor(colors, 1);
    }

    @Override
    @Deprecated
    public Color checkFieldColor(ArrayList<Color> colors, int threshold) {
        if ((colors.get(0) != Color.GRAY && colors.get(0) != Color.BLACK) || (colors.get(1) != Color.GRAY && colors.get(1) != Color.BLACK) || (colors.get(2) != Color.GRAY && colors.get(2) != Color.BLACK) || (colors.get(3) != Color.GRAY && colors.get(3) != Color.BLACK)) {

            int[] calc = {0, 0, 0, 0};

            if (colors.get(0) != Color.GRAY && colors.get(0) != Color.BLACK) { //first color
                calc[0] = 1;
            }
            if (colors.get(1) != Color.GRAY && colors.get(1) != Color.BLACK) { //second color
                if (colors.get(1) == colors.get(0)) { //if same as previous
                    calc[0]++;
                } else {
                    calc[1] = 1;
                }
            }
            if (colors.get(2) != Color.GRAY && colors.get(2) != Color.BLACK) {
                if (colors.get(2) == colors.get(0)) { //if same as first
                    calc[0]++;
                } else if (colors.get(2) == colors.get(1)) {
                    calc[1]++;
                } else {
                    calc[2] = 1;
                }
            }
            if (colors.get(3) != Color.GRAY && colors.get(3) != Color.BLACK) {
                if (colors.get(3) == colors.get(0)) { //if same as first
                    calc[0]++;
                } else if (colors.get(3) == colors.get(1)) {
                    calc[1]++;
                } else if (colors.get(3) == colors.get(2)) {
                    calc[2]++;
                } else {
                    calc[3] = 1;
                }
            }
            List<Integer> maxes = new ArrayList<>(); //create list for max values
            int maxIndex = MathUtils.getIndexOfLargest(calc);
            int max = MathUtils.getMaxt(calc);
            if (max >= threshold) { //check threshold

                //add max values Indexes
                if (calc[maxIndex] == calc[3] && colors.get(3) != Color.GRAY && colors.get(3) != Color.BLACK) {
                    maxes.add(3);
                }
                if (calc[maxIndex] == calc[2] && colors.get(2) != Color.GRAY && colors.get(2) != Color.BLACK) {
                    maxes.add(2);
                }
                if (calc[maxIndex] == calc[1] && colors.get(1) != Color.GRAY && colors.get(1) != Color.BLACK) {
                    maxes.add(1);
                }
                if (calc[maxIndex] == calc[0] && colors.get(0) != Color.GRAY && colors.get(0) != Color.BLACK) {
                    maxes.add(0);
                }


                //get one of maxes - randomly
                Random random = new Random();
                int searched = random.nextInt(maxes.size());

                // return chosen color
                switch (maxes.get(searched)) {
                    case 0:
                        return colors.get(0);
                    case 1:
                        return colors.get(1);
                    case 2:
                        return colors.get(2);
                    case 3:
                        return colors.get(3);
                }
            }
        }
        return null;
    }

    @Override
    @Deprecated
    public ArrayList<Color> getNeighborColors(Space space, int i, int j) {
        ArrayList<Color> colors = new ArrayList<>();
        if (i == 0 || i == space.x - 1 || j == 0 || j == space.y - 1) { //on the border
            if (boundaryConditions == BoundaryConditions.Periodic) {

                if (i == 0) {
                    if (j == 0) { //corner
                        colors.add(space.fields[space.x - 1][j].getColor());
                        colors.add(space.fields[i][j + 1].getColor());
                        colors.add(space.fields[i + 1][j].getColor());
                        colors.add(space.fields[i][space.y - 1].getColor());
                    } else if (j == space.y - 1) { //corner
                        colors.add(space.fields[space.x - 1][j].getColor());
                        colors.add(space.fields[i][0].getColor());
                        colors.add(space.fields[i + 1][j].getColor());
                        colors.add(space.fields[i][j - 1].getColor());
                    } else { //just edge
                        colors.add(space.fields[space.x - 1][j].getColor());
                        colors.add(space.fields[i][j + 1].getColor());
                        colors.add(space.fields[i + 1][j].getColor());
                        colors.add(space.fields[i][j - 1].getColor());
                    }
                } else if (i == space.x - 1) {
                    if (j == 0) { //corner
                        colors.add(space.fields[i - 1][j].getColor());
                        colors.add(space.fields[i][j + 1].getColor());
                        colors.add(space.fields[0][j].getColor());
                        colors.add(space.fields[i][space.y - 1].getColor());
                    } else if (j == space.y - 1) { //corner
                        colors.add(space.fields[i - 1][j].getColor());
                        colors.add(space.fields[i][0].getColor());
                        colors.add(space.fields[0][j].getColor());
                        colors.add(space.fields[i][j - 1].getColor());
                    } else { //just edge
                        colors.add(space.fields[i - 1][j].getColor());
                        colors.add(space.fields[i][j + 1].getColor());
                        colors.add(space.fields[0][j].getColor());
                        colors.add(space.fields[i][j - 1].getColor());
                    }

                } else if (j == 0) {
                    colors.add(space.fields[i - 1][j].getColor());
                    colors.add(space.fields[i][j + 1].getColor());
                    colors.add(space.fields[i + 1][j].getColor());
                    colors.add(space.fields[i][space.y - 1].getColor());

                } else if (j == space.y - 1) {
                    colors.add(space.fields[i - 1][j].getColor());
                    colors.add(space.fields[i][0].getColor());
                    colors.add(space.fields[i + 1][j].getColor());
                    colors.add(space.fields[i][j - 1].getColor());
                }

            } else { //non periodic
                if (i == 0) {
                    if (j == 0) { //corner
                        colors.add(Color.BLACK);
                        colors.add(space.fields[i][j + 1].getColor());
                        colors.add(space.fields[i + 1][j].getColor());
                        colors.add(Color.BLACK);
                    } else if (j == space.y - 1) { //corner
                        colors.add(Color.BLACK);
                        colors.add(Color.BLACK);
                        colors.add(space.fields[i + 1][j].getColor());
                        colors.add(space.fields[i][j - 1].getColor());
                    } else { //just edge
                        colors.add(Color.BLACK);
                        colors.add(space.fields[i][j + 1].getColor());
                        colors.add(space.fields[i + 1][j].getColor());
                        colors.add(space.fields[i][j - 1].getColor());
                    }
                } else if (i == space.x - 1) {
                    if (j == 0) { //corner
                        colors.add(space.fields[i - 1][j].getColor());
                        colors.add(space.fields[i][j + 1].getColor());
                        colors.add(Color.BLACK);
                        colors.add(Color.BLACK);
                    } else if (j == space.y - 1) { //corner
                        colors.add(space.fields[i - 1][j].getColor());
                        colors.add(Color.BLACK);
                        colors.add(Color.BLACK);
                        colors.add(space.fields[i][j - 1].getColor());
                    } else { //just edge
                        colors.add(space.fields[i - 1][j].getColor());
                        colors.add(space.fields[i][j + 1].getColor());
                        colors.add(Color.BLACK);
                        colors.add(space.fields[i][j - 1].getColor());
                    }
                } else if (j == 0) {
                    colors.add(space.fields[i - 1][j].getColor());
                    colors.add(space.fields[i][j + 1].getColor());
                    colors.add(space.fields[i + 1][j].getColor());
                    colors.add(Color.BLACK);

                } else if (j == space.y - 1) {
                    colors.add(space.fields[i - 1][j].getColor());
                    colors.add(Color.BLACK);
                    colors.add(space.fields[i + 1][j].getColor());
                    colors.add(space.fields[i][j - 1].getColor());
                }
            }
        } else { //inside space
            colors.add(space.fields[i - 1][j].getColor());
            colors.add(space.fields[i][j + 1].getColor());
            colors.add(space.fields[i + 1][j].getColor());
            colors.add(space.fields[i][j - 1].getColor());
        }
        return colors;
    }

    @Override
    public Grain fieldGrainCalc(Space space, int i, int j, int phase) {
        // System.out.println("VonNeumann: fieldGrainCalc started");
        ArrayList<Grain> grains = getNeighborGrains(space, i, j, phase);
        //System.out.println("VonNeumann: fieldGrainCalc: got neighbors");
        return checkFieldGrain(grains);

    }

    @Override
    public Grain checkFieldGrain(ArrayList<Grain> grains) {
        return checkFieldGrain(grains, 1);
    }

    @Override
    public Grain checkFieldGrain(ArrayList<Grain> grains, int threshold) {
        if ((grains.get(0).getColor() != Color.GRAY && grains.get(0).getColor() != Color.BLACK)
                || (grains.get(1).getColor() != Color.GRAY && grains.get(1).getColor() != Color.BLACK)
                || (grains.get(2).getColor() != Color.GRAY && grains.get(2).getColor() != Color.BLACK)
                || (grains.get(3).getColor() != Color.GRAY && grains.get(3).getColor() != Color.BLACK)) {

            int[] calc = {0, 0, 0, 0};

            if (grains.get(0).getColor() != Color.GRAY && grains.get(0).getColor() != Color.BLACK) { //first color
                calc[0] = 1;
            }
            if (grains.get(1).getColor() != Color.GRAY && grains.get(1).getColor() != Color.BLACK) { //second color
                if (grains.get(1).getColor() == grains.get(0).getColor()) { //if same as previous
                    calc[0]++;
                } else {
                    calc[1] = 1;
                }
            }
            if (grains.get(2).getColor() != Color.GRAY && grains.get(2).getColor() != Color.BLACK) {
                if (grains.get(2).getColor() == grains.get(0).getColor()) { //if same as first
                    calc[0]++;
                } else if (grains.get(2).getColor() == grains.get(1).getColor()) {
                    calc[1]++;
                } else {
                    calc[2] = 1;
                }
            }
            if (grains.get(3).getColor() != Color.GRAY && grains.get(3).getColor() != Color.BLACK) {
                if (grains.get(3).getColor() == grains.get(0).getColor()) { //if same as first
                    calc[0]++;
                } else if (grains.get(3).getColor() == grains.get(1).getColor()) {
                    calc[1]++;
                } else if (grains.get(3).getColor() == grains.get(2).getColor()) {
                    calc[2]++;
                } else {
                    calc[3] = 1;
                }
            }
            List<Integer> maxes = new ArrayList<>();
            int maxIndex = MathUtils.getIndexOfLargest(calc);
            int max = MathUtils.getMaxt(calc);
            if (max >= threshold) {


                if (calc[maxIndex] == calc[3] && grains.get(3).getColor() != Color.GRAY && grains.get(3).getColor() != Color.BLACK) {
                    maxes.add(3);
                }
                if (calc[maxIndex] == calc[2] && grains.get(2).getColor() != Color.GRAY && grains.get(2).getColor() != Color.BLACK) {
                    maxes.add(2);
                }
                if (calc[maxIndex] == calc[1] && grains.get(1).getColor() != Color.GRAY && grains.get(1).getColor() != Color.BLACK) {
                    maxes.add(1);
                }
                if (calc[maxIndex] == calc[0] && grains.get(0).getColor() != Color.GRAY && grains.get(0).getColor() != Color.BLACK) {
                    maxes.add(0);
                }

                Random random = new Random();
                int searched = random.nextInt(maxes.size());


                switch (maxes.get(searched)) {
                    case 0:
                        return grains.get(0);
                    case 1:
                        return grains.get(1);
                    case 2:
                        return grains.get(2);
                    case 3:
                        return grains.get(3);
                }
            }
        }
        return null;
    }

    @Override
    public ArrayList<Grain> getNeighborGrains(Space space, int i, int j, int phase) {


        ArrayList<Grain> grains = new ArrayList<>();
        if (i == 0 || i == space.x - 1 || j == 0 || j == space.y - 1) {
            if (boundaryConditions == BoundaryConditions.Periodic) {
                if (i == 0) {
                    if (j == 0) { //corner
                        if (space.fields[space.x - 1][j].getPhase() == phase) {
                            grains.add(space.fields[space.x - 1][j].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[i][j + 1].getPhase() == phase) {
                            grains.add(space.fields[i][j + 1].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[i + 1][j].getPhase() == phase) {
                            grains.add(space.fields[i + 1][j].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[i][space.y - 1].getPhase() == phase) {
                            grains.add(space.fields[i][space.y - 1].getParent());
                        } else grains.add(new VoidGrain());


                    } else if (j == space.y - 1) { //corner
                        if (space.fields[space.x - 1][j].getPhase() == phase) {
                            grains.add(space.fields[space.x - 1][j].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[i][0].getPhase() == phase) {
                            grains.add(space.fields[i][0].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[i + 1][j].getPhase() == phase) {
                            grains.add(space.fields[i + 1][j].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[i][j - 1].getPhase() == phase) {
                            grains.add(space.fields[i][j - 1].getParent());
                        } else grains.add(new VoidGrain());
                    } else { //just edge
                        if (space.fields[space.x - 1][j].getPhase() == phase) {
                            grains.add(space.fields[space.x - 1][j].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[i][j + 1].getPhase() == phase) {
                            grains.add(space.fields[i][j + 1].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[i + 1][j].getPhase() == phase) {
                            grains.add(space.fields[i + 1][j].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[i][j - 1].getPhase() == phase) {
                            grains.add(space.fields[i][j - 1].getParent());
                        } else grains.add(new VoidGrain());
                    }
                } else if (i == space.x - 1) {
                    if (j == 0) { //corner
                        if (space.fields[i - 1][j].getPhase() == phase) {
                            grains.add(space.fields[i - 1][j].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[i][j + 1].getPhase() == phase) {
                            grains.add(space.fields[i][j + 1].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[0][j].getPhase() == phase) {
                            grains.add(space.fields[0][j].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[i][space.y - 1].getPhase() == phase) {
                            grains.add(space.fields[i][space.y - 1].getParent());
                        } else grains.add(new VoidGrain());
                    } else if (j == space.y - 1) { //corner
                        if (space.fields[i - 1][j].getPhase() == phase) {
                            grains.add(space.fields[i - 1][j].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[i][0].getPhase() == phase) {
                            grains.add(space.fields[i][0].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[0][j].getPhase() == phase) {
                            grains.add(space.fields[0][j].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[i][j - 1].getPhase() == phase) {
                            grains.add(space.fields[i][j - 1].getParent());
                        } else grains.add(new VoidGrain());


                    } else { //just edge
                        if (space.fields[i - 1][j].getPhase() == phase) {
                            grains.add(space.fields[i - 1][j].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[i][j + 1].getPhase() == phase) {
                            grains.add(space.fields[i][j + 1].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[0][j].getPhase() == phase) {
                            grains.add(space.fields[0][j].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[i][j - 1].getPhase() == phase) {
                            grains.add(space.fields[i][j - 1].getParent());
                        } else grains.add(new VoidGrain());


                    }

                } else if (j == 0) {
                    if (space.fields[i - 1][j].getPhase() == phase) {
                        grains.add(space.fields[i - 1][j].getParent());
                    } else grains.add(new VoidGrain());
                    if (space.fields[i][j + 1].getPhase() == phase) {
                        grains.add(space.fields[i][j + 1].getParent());
                    } else grains.add(new VoidGrain());
                    if (space.fields[i + 1][j].getPhase() == phase) {
                        grains.add(space.fields[i + 1][j].getParent());
                    } else grains.add(new VoidGrain());
                    if (space.fields[i][space.y - 1].getPhase() == phase) {
                        grains.add(space.fields[i][space.y - 1].getParent());
                    } else grains.add(new VoidGrain());


                } else if (j == space.y - 1) {
                    if (space.fields[i - 1][j].getPhase() == phase) {
                        grains.add(space.fields[i - 1][j].getParent());
                    } else grains.add(new VoidGrain());
                    if (space.fields[i][0].getPhase() == phase) {
                        grains.add(space.fields[i][0].getParent());
                    } else grains.add(new VoidGrain());
                    if (space.fields[i + 1][j].getPhase() == phase) {
                        grains.add(space.fields[i + 1][j].getParent());
                    } else grains.add(new VoidGrain());
                    if (space.fields[i][j - 1].getPhase() == phase) {
                        grains.add(space.fields[i][j - 1].getParent());
                    } else grains.add(new VoidGrain());


                }

            } else { //non periodic
                if (i == 0) {
                    if (j == 0) { //corner
                        grains.add(new BorderGrain());
                        if (space.fields[i][j + 1].getPhase() == phase) {
                            grains.add(space.fields[i][j + 1].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[i + 1][j].getPhase() == phase) {
                            grains.add(space.fields[i + 1][j].getParent());
                        } else grains.add(new VoidGrain());


                        grains.add(new BorderGrain());
                    } else if (j == space.y - 1) { //corner
                        grains.add(new BorderGrain());
                        grains.add(new BorderGrain());
                        if (space.fields[i + 1][j].getPhase() == phase) {
                            grains.add(space.fields[i + 1][j].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[i][j - 1].getPhase() == phase) {
                            grains.add(space.fields[i][j - 1].getParent());
                        } else grains.add(new VoidGrain());


                    } else { //just edge
                        grains.add(new BorderGrain());
                        if (space.fields[i][j + 1].getPhase() == phase) {
                            grains.add(space.fields[i][j + 1].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[i + 1][j].getPhase() == phase) {
                            grains.add(space.fields[i + 1][j].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[i][j - 1].getPhase() == phase) {
                            grains.add(space.fields[i][j - 1].getParent());
                        } else grains.add(new VoidGrain());


                    }
                } else if (i == space.x - 1) {
                    if (j == 0) { //corner
                        if (space.fields[i - 1][j].getPhase() == phase) {
                            grains.add(space.fields[i - 1][j].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[i][j + 1].getPhase() == phase) {
                            grains.add(space.fields[i][j + 1].getParent());
                        } else grains.add(new VoidGrain());


                        grains.add(new BorderGrain());
                        grains.add(new BorderGrain());
                    } else if (j == space.y - 1) { //corner
                        if (space.fields[i - 1][j].getPhase() == phase) {
                            grains.add(space.fields[i - 1][j].getParent());
                        } else grains.add(new VoidGrain());

                        grains.add(new BorderGrain());
                        grains.add(new BorderGrain());
                        if (space.fields[i][j - 1].getPhase() == phase) {
                            grains.add(space.fields[i][j - 1].getParent());
                        } else grains.add(new VoidGrain());

                    } else { //just edge
                        if (space.fields[i - 1][j].getPhase() == phase) {
                            grains.add(space.fields[i - 1][j].getParent());
                        } else grains.add(new VoidGrain());
                        if (space.fields[i][j + 1].getPhase() == phase) {
                            grains.add(space.fields[i][j + 1].getParent());
                        } else grains.add(new VoidGrain());
                        grains.add(new BorderGrain());

                        if (space.fields[i][j - 1].getPhase() == phase) {
                            grains.add(space.fields[i][j - 1].getParent());
                        } else grains.add(new VoidGrain());


                    }
                } else if (j == 0) {
                    if (space.fields[i - 1][j].getPhase() == phase) {
                        grains.add(space.fields[i - 1][j].getParent());
                    } else grains.add(new VoidGrain());
                    if (space.fields[i][j + 1].getPhase() == phase) {
                        grains.add(space.fields[i][j + 1].getParent());
                    } else grains.add(new VoidGrain());
                    if (space.fields[i + 1][j].getPhase() == phase) {
                        grains.add(space.fields[i + 1][j].getParent());
                    } else grains.add(new VoidGrain());


                    grains.add(new BorderGrain());

                } else if (j == space.y - 1) {
                    if (space.fields[i - 1][j].getPhase() == phase) {
                        grains.add(space.fields[i - 1][j].getParent());
                    } else grains.add(new VoidGrain());

                    grains.add(new BorderGrain());
                    if (space.fields[i + 1][j].getPhase() == phase) {
                        grains.add(space.fields[i + 1][j].getParent());
                    } else grains.add(new VoidGrain());
                    if (space.fields[i][j - 1].getPhase() == phase) {
                        grains.add(space.fields[i][j - 1].getParent());
                    } else grains.add(new VoidGrain());


                }
            }
        } else {
            if (space.fields[i - 1][j].getPhase() == phase) {
                grains.add(space.fields[i - 1][j].getParent());
            } else grains.add(new VoidGrain());
            if (space.fields[i][j + 1].getPhase() == phase) {
                grains.add(space.fields[i][j + 1].getParent());
            } else grains.add(new VoidGrain());
            if (space.fields[i + 1][j].getPhase() == phase) {
                grains.add(space.fields[i + 1][j].getParent());
            } else grains.add(new VoidGrain());
            if (space.fields[i][j - 1].getPhase() == phase) {
                grains.add(space.fields[i][j - 1].getParent());
            } else grains.add(new VoidGrain());


        }

        return grains;

    }


}
