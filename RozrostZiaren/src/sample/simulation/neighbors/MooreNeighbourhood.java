package sample.simulation.neighbors;

import javafx.scene.paint.Color;
import sample.MathUtils;
import sample.simulation.BoundaryConditions;
import sample.simulation.NeighbourhoodType;
import sample.simulation.Space;
import sample.simulation.grains.BorderGrain;
import sample.simulation.grains.Grain;
import sample.simulation.grains.VoidGrain;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MooreNeighbourhood implements Neighbourhood {

    private NeighbourhoodType neighbourhoodType;
    private BoundaryConditions boundaryConditions;

    public MooreNeighbourhood(NeighbourhoodType neighbourhoodType, BoundaryConditions boundaryConditions) {
        this.neighbourhoodType = neighbourhoodType;
        this.boundaryConditions = boundaryConditions;
    }

    @Override
    @Deprecated
    public Color fieldColorCalc(Space space, int i, int j) {
        ArrayList<Color> colors = getNeighborColors(space, i, j);
        return checkFieldColor(colors);
    }

    @Override
    @Deprecated
    public Color checkFieldColor(ArrayList<Color> colors) {
        return checkFieldColor(colors, 1);
    }

    @Override
    @Deprecated
    public Color checkFieldColor(ArrayList<Color> colors, int threshold) {
        //System.out.println("Inside specific field check | size: "+colors.size());
        //  if (colors.size() == 8) {

        if ((colors.get(0) != Color.GRAY && colors.get(0) != Color.BLACK) || (colors.get(1) != Color.GRAY && colors.get(1) != Color.BLACK)
                || (colors.get(2) != Color.GRAY && colors.get(2) != Color.BLACK) || (colors.get(3) != Color.GRAY && colors.get(3) != Color.BLACK)
                || (colors.get(4) != Color.GRAY && colors.get(4) != Color.BLACK) || (colors.get(5) != Color.GRAY && colors.get(5) != Color.BLACK)
                || (colors.get(6) != Color.GRAY && colors.get(6) != Color.BLACK) || (colors.get(7) != Color.GRAY && colors.get(7) != Color.BLACK)) {
          /*
            boolean areNeighbors = false;
            for (Color color : colors) {
                if (color != Color.GRAY && color != Color.BLACK) {
                    areNeighbors = true;
                }
            }
            if (areNeighbors) {
*/


            int[] calc = {0, 0, 0, 0, 0, 0, 0, 0};
            //System.out.println("Matrix created");
            if (colors.get(0) != Color.GRAY && colors.get(0) != Color.BLACK) {
                calc[0] = 1;
            }
            if (colors.get(1) != Color.GRAY && colors.get(1) != Color.BLACK) {
                //if same as previous
                if (colors.get(1) == colors.get(0)) {
                    calc[0]++;
                } else {
                    calc[1] = 1;
                }
            }
            if (colors.get(2) != Color.GRAY && colors.get(2) != Color.BLACK) {
                if (colors.get(2) == colors.get(0)) {
                    calc[0]++;
                } else if (colors.get(2) == colors.get(1)) {
                    calc[1]++;
                } else {
                    calc[2] = 1;
                }
            }
            if (colors.get(3) != Color.GRAY && colors.get(3) != Color.BLACK) {
                if (colors.get(3) == colors.get(0)) {
                    calc[0]++;
                } else if (colors.get(3) == colors.get(1)) {
                    calc[1]++;
                } else if (colors.get(3) == colors.get(2)) {
                    calc[2]++;
                } else {
                    calc[3] = 1;
                }
            }
            if (colors.get(4) != Color.GRAY && colors.get(4) != Color.BLACK) {
                if (colors.get(4) == colors.get(0)) {
                    calc[0]++;
                } else if (colors.get(4) == colors.get(1)) {
                    calc[1]++;
                } else if (colors.get(4) == colors.get(2)) {
                    calc[2]++;
                } else if (colors.get(4) == colors.get(3)) {
                    calc[3]++;
                } else {
                    calc[4] = 1;
                }
            }
            if (colors.get(5) != Color.GRAY && colors.get(5) != Color.BLACK) {
                if (colors.get(5) == colors.get(0)) {
                    calc[0]++;
                } else if (colors.get(5) == colors.get(1)) {
                    calc[1]++;
                } else if (colors.get(5) == colors.get(2)) {
                    calc[2]++;
                } else if (colors.get(5) == colors.get(3)) {
                    calc[3]++;
                } else if (colors.get(5) == colors.get(4)) {
                    calc[4]++;
                } else {
                    calc[5] = 1;
                }
            }
            if (colors.get(6) != Color.GRAY && colors.get(6) != Color.BLACK) {
                if (colors.get(6) == colors.get(0)) {
                    calc[0]++;
                } else if (colors.get(6) == colors.get(1)) {
                    calc[1]++;
                } else if (colors.get(6) == colors.get(2)) {
                    calc[2]++;
                } else if (colors.get(6) == colors.get(3)) {
                    calc[3]++;
                } else if (colors.get(6) == colors.get(4)) {
                    calc[4]++;
                } else if (colors.get(6) == colors.get(5)) {
                    calc[5]++;
                } else {
                    calc[6] = 1;
                }
            }
            if (colors.get(7) != Color.GRAY && colors.get(7) != Color.BLACK) {
                //if same as firs
                if (colors.get(7) == colors.get(0)) {
                    calc[0]++;
                } else if (colors.get(7) == colors.get(1)) {
                    calc[1]++;
                } else if (colors.get(7) == colors.get(2)) {
                    calc[2]++;
                } else if (colors.get(7) == colors.get(3)) {
                    calc[3]++;
                } else if (colors.get(7) == colors.get(4)) {
                    calc[4]++;
                } else if (colors.get(7) == colors.get(5)) {
                    calc[5]++;
                } else if (colors.get(7) == colors.get(6)) {
                    calc[6]++;
                } else {
                    calc[7] = 1;
                }
            }
            //System.out.println("Calculated colors");
            List<Integer> maxes = new ArrayList<>(); //create list for max values
            int maxIndex = MathUtils.getIndexOfLargest(calc);

            //int max=Integer.parseInt(Collections.max((List)Arrays.asList(calc)).toString());
            // System.out.println("Max is "+max+" where threshold is "+threshold);
            int max = MathUtils.getMaxt(calc);
            if (max >= threshold) { //check threshold

                //add max values Indexes
                if (calc[maxIndex] == calc[7] && colors.get(7) != Color.GRAY && colors.get(7) != Color.BLACK) {
                    maxes.add(7);
                }
                if (calc[maxIndex] == calc[6] && colors.get(6) != Color.GRAY && colors.get(6) != Color.BLACK) {
                    maxes.add(6);
                }
                if (calc[maxIndex] == calc[5] && colors.get(5) != Color.GRAY && colors.get(5) != Color.BLACK) {
                    maxes.add(5);
                }
                if (calc[maxIndex] == calc[4] && colors.get(4) != Color.GRAY && colors.get(4) != Color.BLACK) {
                    maxes.add(4);
                }
                if (calc[maxIndex] == calc[3] && colors.get(3) != Color.GRAY && colors.get(3) != Color.BLACK) {
                    maxes.add(3);
                }
                if (calc[maxIndex] == calc[2] && colors.get(2) != Color.GRAY && colors.get(2) != Color.BLACK) {
                    maxes.add(2);
                }
                if (calc[maxIndex] == calc[1] && colors.get(1) != Color.GRAY && colors.get(1) != Color.BLACK) {
                    maxes.add(1);
                }
                if (calc[maxIndex] == calc[0] && colors.get(0) != Color.GRAY && colors.get(0) != Color.BLACK) {
                    maxes.add(0);
                }
                //System.out.println("Maxes gathered");
                Random random = new Random();
                int searched = random.nextInt(maxes.size());
                // System.out.println("Return max");
                // return chosen color
                switch (maxes.get(searched)) {
                    case 0:

                        return colors.get(0);
                    case 1:
                        return colors.get(1);
                    case 2:
                        return colors.get(2);
                    case 3:
                        return colors.get(3);
                    case 4:
                        return colors.get(4);
                    case 5:
                        return colors.get(5);
                    case 6:
                        return colors.get(6);
                    case 7:
                        return colors.get(7);
                }
            }
        }
        return null;
    }

    @Deprecated
    @Override
    public ArrayList<Color> getNeighborColors(Space space, int i, int j) {
        ArrayList<Color> colors = new ArrayList<>();

        if (i == 0 || i == space.x - 1 || j == 0 || j == space.y - 1) { //on the border
            if (boundaryConditions == BoundaryConditions.Periodic) {
                if (i == 0) {
                    if (j == 0) { //corner
                        colors.add(space.fields[space.x - 1][space.y - 1].getColor());
                        colors.add(space.fields[space.x - 1][j].getColor());
                        colors.add(space.fields[space.x - 1][j + 1].getColor());
                        colors.add(space.fields[i][j + 1].getColor());
                        colors.add(space.fields[i + 1][j + 1].getColor());
                        colors.add(space.fields[i + 1][j].getColor());
                        colors.add(space.fields[i + 1][space.y - 1].getColor());
                        colors.add(space.fields[i][space.y - 1].getColor());
                    } else if (j == space.y - 1) { //corner
                        colors.add(space.fields[space.x - 1][j - 1].getColor());
                        colors.add(space.fields[space.x - 1][j].getColor());
                        colors.add(space.fields[space.x - 1][0].getColor());
                        colors.add(space.fields[i][0].getColor());
                        colors.add(space.fields[i + 1][0].getColor());
                        colors.add(space.fields[i + 1][j].getColor());
                        colors.add(space.fields[i + 1][j - 1].getColor());
                        colors.add(space.fields[i][j - 1].getColor());
                    } else { //just edge
                        colors.add(space.fields[space.x - 1][j - 1].getColor());
                        colors.add(space.fields[space.x - 1][j].getColor());
                        colors.add(space.fields[space.x - 1][j + 1].getColor());
                        colors.add(space.fields[i][j + 1].getColor());
                        colors.add(space.fields[i + 1][j + 1].getColor());
                        colors.add(space.fields[i + 1][j].getColor());
                        colors.add(space.fields[i + 1][j - 1].getColor());
                        colors.add(space.fields[i][j - 1].getColor());
                    }
                } else if (i == space.x - 1) {
                    if (j == 0) { //corner
                        colors.add(space.fields[i - 1][space.y - 1].getColor());
                        colors.add(space.fields[i - 1][j].getColor());
                        colors.add(space.fields[i - 1][j + 1].getColor());
                        colors.add(space.fields[i][j + 1].getColor());
                        colors.add(space.fields[0][j + 1].getColor());
                        colors.add(space.fields[0][j].getColor());
                        colors.add(space.fields[0][space.y - 1].getColor());
                        colors.add(space.fields[i][space.y - 1].getColor());
                    } else if (j == space.y - 1) { //corner
                        colors.add(space.fields[i - 1][j - 1].getColor());
                        colors.add(space.fields[i - 1][j].getColor());
                        colors.add(space.fields[i - 1][0].getColor());
                        colors.add(space.fields[i][0].getColor());
                        colors.add(space.fields[0][0].getColor());
                        colors.add(space.fields[0][j].getColor());
                        colors.add(space.fields[0][j - 1].getColor());
                        colors.add(space.fields[i][j - 1].getColor());
                    } else { //just edge
                        colors.add(space.fields[i - 1][j - 1].getColor());
                        colors.add(space.fields[i - 1][j].getColor());
                        colors.add(space.fields[i - 1][j + 1].getColor());
                        colors.add(space.fields[i][j + 1].getColor());
                        colors.add(space.fields[0][j + 1].getColor());
                        colors.add(space.fields[0][j].getColor());
                        colors.add(space.fields[0][j - 1].getColor());
                        colors.add(space.fields[i][j - 1].getColor());
                    }
                } else if (j == 0) {
                    colors.add(space.fields[i - 1][space.y - 1].getColor());
                    colors.add(space.fields[i - 1][j].getColor());
                    colors.add(space.fields[i - 1][j + 1].getColor());
                    colors.add(space.fields[i][j + 1].getColor());
                    colors.add(space.fields[i + 1][j + 1].getColor());
                    colors.add(space.fields[i + 1][j].getColor());
                    colors.add(space.fields[i + 1][space.y - 1].getColor());
                    colors.add(space.fields[i][space.y - 1].getColor());

                } else if (j == space.y - 1) {
                    colors.add(space.fields[i - 1][j - 1].getColor());
                    colors.add(space.fields[i - 1][j].getColor());
                    colors.add(space.fields[i - 1][0].getColor());
                    colors.add(space.fields[i][0].getColor());
                    colors.add(space.fields[i + 1][0].getColor());
                    colors.add(space.fields[i + 1][j].getColor());
                    colors.add(space.fields[i + 1][j - 1].getColor());
                    colors.add(space.fields[i][j - 1].getColor());
                }
            } else { //non periodic
                if (i == 0) {
                    if (j == 0) { //corner
                        colors.add(Color.BLACK);
                        colors.add(Color.BLACK);
                        colors.add(Color.BLACK);
                        colors.add(space.fields[i][j + 1].getColor());
                        colors.add(space.fields[i + 1][j + 1].getColor());
                        colors.add(space.fields[i + 1][j].getColor());
                        colors.add(Color.BLACK);
                        colors.add(Color.BLACK);
                    } else if (j == space.y - 1) { //corner
                        colors.add(Color.BLACK);
                        colors.add(Color.BLACK);
                        colors.add(Color.BLACK);
                        colors.add(Color.BLACK);
                        colors.add(Color.BLACK);
                        colors.add(space.fields[i + 1][j].getColor());
                        colors.add(space.fields[i + 1][j - 1].getColor());
                        colors.add(space.fields[i][j - 1].getColor());
                    } else { //just edge
                        colors.add(Color.BLACK);
                        colors.add(Color.BLACK);
                        colors.add(Color.BLACK);
                        colors.add(space.fields[i][j + 1].getColor());
                        colors.add(space.fields[i + 1][j + 1].getColor());
                        colors.add(space.fields[i + 1][j].getColor());
                        colors.add(space.fields[i + 1][j - 1].getColor());
                        colors.add(space.fields[i][j - 1].getColor());
                    }
                } else if (i == space.x - 1) {
                    if (j == 0) { //corner
                        colors.add(Color.BLACK);
                        colors.add(space.fields[i - 1][j].getColor());
                        colors.add(space.fields[i - 1][j + 1].getColor());
                        colors.add(space.fields[i][j + 1].getColor());
                        colors.add(Color.BLACK);
                        colors.add(Color.BLACK);
                        colors.add(Color.BLACK);
                        colors.add(Color.BLACK);
                    } else if (j == space.y - 1) { //corner
                        colors.add(space.fields[i - 1][j - 1].getColor());
                        colors.add(space.fields[i - 1][j].getColor());
                        colors.add(Color.BLACK);
                        colors.add(Color.BLACK);
                        colors.add(Color.BLACK);
                        colors.add(Color.BLACK);
                        colors.add(Color.BLACK);
                        colors.add(space.fields[i][j - 1].getColor());
                    } else { //just edge
                        colors.add(space.fields[i - 1][j - 1].getColor());
                        colors.add(space.fields[i - 1][j].getColor());
                        colors.add(space.fields[i - 1][j + 1].getColor());
                        colors.add(space.fields[i][j + 1].getColor());
                        colors.add(Color.BLACK);
                        colors.add(Color.BLACK);
                        colors.add(Color.BLACK);
                        colors.add(space.fields[i][j - 1].getColor());
                    }

                } else if (j == 0) {
                    colors.add(Color.BLACK);
                    colors.add(space.fields[i - 1][j].getColor());
                    colors.add(space.fields[i - 1][j + 1].getColor());
                    colors.add(space.fields[i][j + 1].getColor());
                    colors.add(space.fields[i + 1][j + 1].getColor());
                    colors.add(space.fields[i + 1][j].getColor());
                    colors.add(Color.BLACK);
                    colors.add(Color.BLACK);

                } else if (j == space.y - 1) {
                    colors.add(space.fields[i - 1][j - 1].getColor());
                    colors.add(space.fields[i - 1][j].getColor());
                    colors.add(Color.BLACK);
                    colors.add(Color.BLACK);
                    colors.add(Color.BLACK);
                    colors.add(space.fields[i + 1][j].getColor());
                    colors.add(space.fields[i + 1][j - 1].getColor());
                    colors.add(space.fields[i][j - 1].getColor());
                }
            }
        } else { //inside space
            colors.add(space.fields[i - 1][j - 1].getColor());
            colors.add(space.fields[i - 1][j].getColor());
            colors.add(space.fields[i - 1][j + 1].getColor());
            colors.add(space.fields[i][j + 1].getColor());
            colors.add(space.fields[i + 1][j + 1].getColor());
            colors.add(space.fields[i + 1][j].getColor());
            colors.add(space.fields[i + 1][j - 1].getColor());
            colors.add(space.fields[i][j - 1].getColor());
        }
        return colors;
    }


    @Override
    public Grain fieldGrainCalc(Space space, int i, int j, int phase) {
        ArrayList<Grain> grains = getNeighborGrains(space, i, j, phase);
        return checkFieldGrain(grains);
    }

    @Override
    public Grain checkFieldGrain(ArrayList<Grain> grains) {
        return checkFieldGrain(grains, 1);
    }

    @Override
    public Grain checkFieldGrain(ArrayList<Grain> grains, int threshold) {

        if ((grains.get(0).getColor() != Color.GRAY && grains.get(0).getColor() != Color.BLACK) || (grains.get(1).getColor() != Color.GRAY && grains.get(1).getColor() != Color.BLACK)
                || (grains.get(2).getColor() != Color.GRAY && grains.get(2).getColor() != Color.BLACK) || (grains.get(3).getColor() != Color.GRAY && grains.get(3).getColor() != Color.BLACK)
                || (grains.get(4).getColor() != Color.GRAY && grains.get(4).getColor() != Color.BLACK) || (grains.get(5).getColor() != Color.GRAY && grains.get(5).getColor() != Color.BLACK)
                || (grains.get(6).getColor() != Color.GRAY && grains.get(6).getColor() != Color.BLACK) || (grains.get(7).getColor() != Color.GRAY && grains.get(7).getColor() != Color.BLACK)) {
          /*
            boolean areNeighbors = false;
            for (Color color : colors) {
                if (color != Color.GRAY && color != Color.BLACK) {
                    areNeighbors = true;
                }
            }
            if (areNeighbors) {
*/


            int[] calc = {0, 0, 0, 0, 0, 0, 0, 0};
            //System.out.println("Matrix created");
            if (grains.get(0).getColor() != Color.GRAY && grains.get(0).getColor() != Color.BLACK) {
                calc[0] = 1;
            }
            if (grains.get(1).getColor() != Color.GRAY && grains.get(1).getColor() != Color.BLACK) {
                //if same as previous
                if (grains.get(1).getColor() == grains.get(0).getColor()) {
                    calc[0]++;
                } else {
                    calc[1] = 1;
                }
            }
            if (grains.get(2).getColor() != Color.GRAY && grains.get(2).getColor() != Color.BLACK) {
                if (grains.get(2).getColor() == grains.get(0).getColor()) {
                    calc[0]++;
                } else if (grains.get(2).getColor() == grains.get(1).getColor()) {
                    calc[1]++;
                } else {
                    calc[2] = 1;
                }
            }
            if (grains.get(3).getColor() != Color.GRAY && grains.get(3).getColor() != Color.BLACK) {
                if (grains.get(3).getColor() == grains.get(0).getColor()) {
                    calc[0]++;
                } else if (grains.get(3).getColor() == grains.get(1).getColor()) {
                    calc[1]++;
                } else if (grains.get(3).getColor() == grains.get(2).getColor()) {
                    calc[2]++;
                } else {
                    calc[3] = 1;
                }
            }
            if (grains.get(4).getColor() != Color.GRAY && grains.get(4).getColor() != Color.BLACK) {
                if (grains.get(4).getColor() == grains.get(0).getColor()) {
                    calc[0]++;
                } else if (grains.get(4).getColor() == grains.get(1).getColor()) {
                    calc[1]++;
                } else if (grains.get(4).getColor() == grains.get(2).getColor()) {
                    calc[2]++;
                } else if (grains.get(4).getColor() == grains.get(3).getColor()) {
                    calc[3]++;
                } else {
                    calc[4] = 1;
                }
            }
            if (grains.get(5).getColor() != Color.GRAY && grains.get(5).getColor() != Color.BLACK) {
                if (grains.get(5).getColor() == grains.get(0).getColor()) {
                    calc[0]++;
                } else if (grains.get(5).getColor() == grains.get(1).getColor()) {
                    calc[1]++;
                } else if (grains.get(5).getColor() == grains.get(2).getColor()) {
                    calc[2]++;
                } else if (grains.get(5).getColor() == grains.get(3).getColor()) {
                    calc[3]++;
                } else if (grains.get(5).getColor() == grains.get(4).getColor()) {
                    calc[4]++;
                } else {
                    calc[5] = 1;
                }
            }
            if (grains.get(6).getColor() != Color.GRAY && grains.get(6).getColor() != Color.BLACK) {
                if (grains.get(6).getColor() == grains.get(0).getColor()) {
                    calc[0]++;
                } else if (grains.get(6).getColor() == grains.get(1).getColor()) {
                    calc[1]++;
                } else if (grains.get(6).getColor() == grains.get(2).getColor()) {
                    calc[2]++;
                } else if (grains.get(6).getColor() == grains.get(3).getColor()) {
                    calc[3]++;
                } else if (grains.get(6).getColor() == grains.get(4).getColor()) {
                    calc[4]++;
                } else if (grains.get(6).getColor() == grains.get(5).getColor()) {
                    calc[5]++;
                } else {
                    calc[6] = 1;
                }
            }
            if (grains.get(7).getColor() != Color.GRAY && grains.get(7).getColor() != Color.BLACK) {
                //if same as firs
                if (grains.get(7).getColor() == grains.get(0).getColor()) {
                    calc[0]++;
                } else if (grains.get(7).getColor() == grains.get(1).getColor()) {
                    calc[1]++;
                } else if (grains.get(7).getColor() == grains.get(2).getColor()) {
                    calc[2]++;
                } else if (grains.get(7).getColor() == grains.get(3).getColor()) {
                    calc[3]++;
                } else if (grains.get(7).getColor() == grains.get(4).getColor()) {
                    calc[4]++;
                } else if (grains.get(7).getColor() == grains.get(5).getColor()) {
                    calc[5]++;
                } else if (grains.get(7).getColor() == grains.get(6).getColor()) {
                    calc[6]++;
                } else {
                    calc[7] = 1;
                }
            }
            //System.out.println("Calculated colors");
            List<Integer> maxes = new ArrayList<>(); //create list for max values
            int maxIndex = MathUtils.getIndexOfLargest(calc);

            //int max=Integer.parseInt(Collections.max((List)Arrays.asList(calc)).toString());
            // System.out.println("Max is "+max+" where threshold is "+threshold);
            int max = MathUtils.getMaxt(calc);
            if (max >= threshold) { //check threshold

                //add max values Indexes
                if (calc[maxIndex] == calc[7] && grains.get(7).getColor() != Color.GRAY && grains.get(7).getColor() != Color.BLACK) {
                    maxes.add(7);
                }
                if (calc[maxIndex] == calc[6] && grains.get(6).getColor() != Color.GRAY && grains.get(6).getColor() != Color.BLACK) {
                    maxes.add(6);
                }
                if (calc[maxIndex] == calc[5] && grains.get(5).getColor() != Color.GRAY && grains.get(5).getColor() != Color.BLACK) {
                    maxes.add(5);
                }
                if (calc[maxIndex] == calc[4] && grains.get(4).getColor() != Color.GRAY && grains.get(4).getColor() != Color.BLACK) {
                    maxes.add(4);
                }
                if (calc[maxIndex] == calc[3] && grains.get(3).getColor() != Color.GRAY && grains.get(3).getColor() != Color.BLACK) {
                    maxes.add(3);
                }
                if (calc[maxIndex] == calc[2] && grains.get(2).getColor() != Color.GRAY && grains.get(2).getColor() != Color.BLACK) {
                    maxes.add(2);
                }
                if (calc[maxIndex] == calc[1] && grains.get(1).getColor() != Color.GRAY && grains.get(1).getColor() != Color.BLACK) {
                    maxes.add(1);
                }
                if (calc[maxIndex] == calc[0] && grains.get(0).getColor() != Color.GRAY && grains.get(0).getColor() != Color.BLACK) {
                    maxes.add(0);
                }
                //System.out.println("Maxes gathered");
                Random random = new Random();
                int searched = random.nextInt(maxes.size());
                // System.out.println("Return max");
                // return chosen color
                switch (maxes.get(searched)) {
                    case 0:

                        return grains.get(0);
                    case 1:
                        return grains.get(1);
                    case 2:
                        return grains.get(2);
                    case 3:
                        return grains.get(3);
                    case 4:
                        return grains.get(4);
                    case 5:
                        return grains.get(5);
                    case 6:
                        return grains.get(6);
                    case 7:
                        return grains.get(7);
                }
            }
        }
        return null;
    }

    @Override
    public ArrayList<Grain> getNeighborGrains(Space space, int i, int j, int phase) {
        ArrayList<Grain> grains = new ArrayList<>();

        if (i == 0 || i == space.x - 1 || j == 0 || j == space.y - 1) { //on the border
            if (boundaryConditions == BoundaryConditions.Periodic) {
                if (i == 0) {
                    if (j == 0) { //corner
                        if (space.fields[space.x - 1][space.y - 1].getParent().getPhase() == phase)
                            grains.add(space.fields[space.x - 1][space.y - 1].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[space.x - 1][j].getParent().getPhase() == phase)
                            grains.add(space.fields[space.x - 1][j].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[space.x - 1][j + 1].getParent().getPhase() == phase)
                            grains.add(space.fields[space.x - 1][j + 1].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[i][j + 1].getParent().getPhase() == phase)
                            grains.add(space.fields[i][j + 1].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[i + 1][j + 1].getParent().getPhase() == phase)
                            grains.add(space.fields[i + 1][j + 1].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[i + 1][j].getParent().getPhase() == phase)
                            grains.add(space.fields[i + 1][j].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[i + 1][space.y - 1].getParent().getPhase() == phase)
                            grains.add(space.fields[i + 1][space.y - 1].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[i][space.y - 1].getParent().getPhase() == phase)
                            grains.add(space.fields[i][space.y - 1].getParent());
                        else
                            grains.add(new VoidGrain());
                            /*
                        if(.getPhase() == phase)

                            else
                            grains.add(new VoidGrain());
                        */

                    } else if (j == space.y - 1) { //corner
                        if (space.fields[space.x - 1][j - 1].getParent().getPhase() == phase)
                            grains.add(space.fields[space.x - 1][j - 1].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[space.x - 1][j].getParent().getPhase() == phase)
                            grains.add(space.fields[space.x - 1][j].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[space.x - 1][0].getParent().getPhase() == phase)
                            grains.add(space.fields[space.x - 1][0].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[i][0].getParent().getPhase() == phase)
                            grains.add(space.fields[i][0].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[i + 1][0].getParent().getPhase() == phase)
                            grains.add(space.fields[i + 1][0].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[i + 1][j].getParent().getPhase() == phase)
                            grains.add(space.fields[i + 1][j].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[i + 1][j - 1].getParent().getPhase() == phase)
                            grains.add(space.fields[i + 1][j - 1].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[i][j - 1].getParent().getPhase() == phase)
                            grains.add(space.fields[i][j - 1].getParent());
                        else
                            grains.add(new VoidGrain());

                    } else { //just edge
                        if (space.fields[space.x - 1][j - 1].getParent().getPhase() == phase)
                            grains.add(space.fields[space.x - 1][j - 1].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[space.x - 1][j].getParent().getPhase() == phase)
                            grains.add(space.fields[space.x - 1][j].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[space.x - 1][j + 1].getParent().getPhase() == phase)
                            grains.add(space.fields[space.x - 1][j + 1].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[i][j + 1].getParent().getPhase() == phase)
                            grains.add(space.fields[i][j + 1].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[i + 1][j + 1].getParent().getPhase() == phase)
                            grains.add(space.fields[i + 1][j + 1].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[i + 1][j].getParent().getPhase() == phase)
                            grains.add(space.fields[i + 1][j].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[i + 1][j - 1].getParent().getPhase() == phase)
                            grains.add(space.fields[i + 1][j - 1].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[i][j - 1].getParent().getPhase() == phase)
                            grains.add(space.fields[i][j - 1].getParent());
                        else
                            grains.add(new VoidGrain());


                    }
                } else if (i == space.x - 1) {
                    if (j == 0) { //corner
                        if (space.fields[i - 1][space.y - 1].getParent().getPhase() == phase)
                            grains.add(space.fields[i - 1][space.y - 1].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[i - 1][j].getParent().getPhase() == phase)
                            grains.add(space.fields[i - 1][j].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[i - 1][j + 1].getParent().getPhase() == phase)
                            grains.add(space.fields[i - 1][j + 1].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[i][j + 1].getParent().getPhase() == phase)
                            grains.add(space.fields[i][j + 1].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[0][j + 1].getParent().getPhase() == phase)
                            grains.add(space.fields[0][j + 1].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[0][j].getParent().getPhase() == phase)
                            grains.add(space.fields[0][j].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[0][space.y - 1].getParent().getPhase() == phase)
                            grains.add(space.fields[0][space.y - 1].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[i][space.y - 1].getParent().getPhase() == phase)
                            grains.add(space.fields[i][space.y - 1].getParent());
                        else
                            grains.add(new VoidGrain());

                    } else if (j == space.y - 1) { //corner
                        if (space.fields[i - 1][j - 1].getParent().getPhase() == phase)
                            grains.add(space.fields[i - 1][j - 1].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[i - 1][j].getParent().getPhase() == phase)
                            grains.add(space.fields[i - 1][j].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[i - 1][0].getParent().getPhase() == phase)
                            grains.add(space.fields[i - 1][0].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[i][0].getParent().getPhase() == phase)
                            grains.add(space.fields[i][0].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[0][0].getParent().getPhase() == phase)
                            grains.add(space.fields[0][0].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[0][j].getParent().getPhase() == phase)
                            grains.add(space.fields[0][j].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[0][j - 1].getParent().getPhase() == phase)
                            grains.add(space.fields[0][j - 1].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[i][j - 1].getParent().getPhase() == phase)
                            grains.add(space.fields[i][j - 1].getParent());
                        else
                            grains.add(new VoidGrain());

                    } else { //just edge
                        if (space.fields[i - 1][j - 1].getParent().getPhase() == phase)
                            grains.add(space.fields[i - 1][j - 1].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[i - 1][j].getParent().getPhase() == phase)
                            grains.add(space.fields[i - 1][j].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[i - 1][j + 1].getParent().getPhase() == phase)
                            grains.add(space.fields[i - 1][j + 1].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[i][j + 1].getParent().getPhase() == phase)
                            grains.add(space.fields[i][j + 1].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[0][j + 1].getParent().getPhase() == phase)
                            grains.add(space.fields[0][j + 1].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[0][j].getParent().getPhase() == phase)
                            grains.add(space.fields[0][j].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[0][j - 1].getParent().getPhase() == phase)
                            grains.add(space.fields[0][j - 1].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[i][j - 1].getParent().getPhase() == phase)
                            grains.add(space.fields[i][j - 1].getParent());
                        else
                            grains.add(new VoidGrain());

                    }
                } else if (j == 0) {
                    if (space.fields[i - 1][space.y - 1].getParent().getPhase() == phase)
                        grains.add(space.fields[i - 1][space.y - 1].getParent());
                    else
                        grains.add(new VoidGrain());
                    if (space.fields[i - 1][j].getParent().getPhase() == phase)
                        grains.add(space.fields[i - 1][j].getParent());
                    else
                        grains.add(new VoidGrain());
                    if (space.fields[i - 1][j + 1].getParent().getPhase() == phase)
                        grains.add(space.fields[i - 1][j + 1].getParent());
                    else
                        grains.add(new VoidGrain());
                    if (space.fields[i][j + 1].getParent().getPhase() == phase)
                        grains.add(space.fields[i][j + 1].getParent());
                    else
                        grains.add(new VoidGrain());
                    if (space.fields[i + 1][j + 1].getParent().getPhase() == phase)
                        grains.add(space.fields[i + 1][j + 1].getParent());
                    else
                        grains.add(new VoidGrain());
                    if (space.fields[i + 1][j].getParent().getPhase() == phase)
                        grains.add(space.fields[i + 1][j].getParent());
                    else
                        grains.add(new VoidGrain());
                    if (space.fields[i + 1][space.y - 1].getParent().getPhase() == phase)
                        grains.add(space.fields[i + 1][space.y - 1].getParent());
                    else
                        grains.add(new VoidGrain());
                    if (space.fields[i][space.y - 1].getParent().getPhase() == phase)
                        grains.add(space.fields[i][space.y - 1].getParent());
                    else
                        grains.add(new VoidGrain());


                } else if (j == space.y - 1) {
                    if (space.fields[i - 1][j - 1].getParent().getPhase() == phase)
                        grains.add(space.fields[i - 1][j - 1].getParent());
                    else
                        grains.add(new VoidGrain());
                    if (space.fields[i - 1][j].getParent().getPhase() == phase)
                        grains.add(space.fields[i - 1][j].getParent());
                    else
                        grains.add(new VoidGrain());
                    if (space.fields[i - 1][0].getParent().getPhase() == phase)
                        grains.add(space.fields[i - 1][0].getParent());
                    else
                        grains.add(new VoidGrain());
                    if (space.fields[i][0].getParent().getPhase() == phase)
                        grains.add(space.fields[i][0].getParent());
                    else
                        grains.add(new VoidGrain());
                    if (space.fields[i + 1][0].getParent().getPhase() == phase)
                        grains.add(space.fields[i + 1][0].getParent());
                    else
                        grains.add(new VoidGrain());
                    if (space.fields[i + 1][j].getParent().getPhase() == phase)
                        grains.add(space.fields[i + 1][j].getParent());
                    else
                        grains.add(new VoidGrain());
                    if (space.fields[i + 1][j - 1].getParent().getPhase() == phase)
                        grains.add(space.fields[i + 1][j - 1].getParent());
                    else
                        grains.add(new VoidGrain());
                    if (space.fields[i][j - 1].getParent().getPhase() == phase)
                        grains.add(space.fields[i][j - 1].getParent());
                    else
                        grains.add(new VoidGrain());

                }
            } else { //non periodic
                if (i == 0) {
                    if (j == 0) { //corner
                        grains.add(new BorderGrain());
                        grains.add(new BorderGrain());
                        grains.add(new BorderGrain());
                        if (space.fields[i][j + 1].getParent().getPhase() == phase)
                            grains.add(space.fields[i][j + 1].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[i + 1][j + 1].getParent().getPhase() == phase)
                            grains.add(space.fields[i + 1][j + 1].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[i + 1][j].getParent().getPhase() == phase)
                            grains.add(space.fields[i + 1][j].getParent());
                        else
                            grains.add(new VoidGrain());


                        grains.add(new BorderGrain());
                        grains.add(new BorderGrain());
                    } else if (j == space.y - 1) { //corner
                        grains.add(new BorderGrain());
                        grains.add(new BorderGrain());
                        grains.add(new BorderGrain());
                        grains.add(new BorderGrain());
                        grains.add(new BorderGrain());
                        if (space.fields[i + 1][j].getParent().getPhase() == phase)
                            grains.add(space.fields[i + 1][j].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[i + 1][j - 1].getParent().getPhase() == phase)
                            grains.add(space.fields[i + 1][j - 1].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[i][j - 1].getParent().getPhase() == phase)
                            grains.add(space.fields[i][j - 1].getParent());
                        else
                            grains.add(new VoidGrain());


                    } else { //just edge
                        grains.add(new BorderGrain());
                        grains.add(new BorderGrain());
                        grains.add(new BorderGrain());
                        if (space.fields[i][j + 1].getParent().getPhase() == phase)
                            grains.add(space.fields[i][j + 1].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[i + 1][j + 1].getParent().getPhase() == phase)
                            grains.add(space.fields[i + 1][j + 1].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[i + 1][j].getParent().getPhase() == phase)
                            grains.add(space.fields[i + 1][j].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[i + 1][j - 1].getParent().getPhase() == phase)
                            grains.add(space.fields[i + 1][j - 1].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[i][j - 1].getParent().getPhase() == phase)
                            grains.add(space.fields[i][j - 1].getParent());
                        else
                            grains.add(new VoidGrain());

                    }
                } else if (i == space.x - 1) {
                    if (j == 0) { //corner
                        grains.add(new BorderGrain());
                        if (space.fields[i - 1][j].getParent().getPhase() == phase)
                            grains.add(space.fields[i - 1][j].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[i - 1][j + 1].getParent().getPhase() == phase)
                            grains.add(space.fields[i - 1][j + 1].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[i][j + 1].getParent().getPhase() == phase)
                            grains.add(space.fields[i][j + 1].getParent());
                        else
                            grains.add(new VoidGrain());

                        grains.add(new BorderGrain());
                        grains.add(new BorderGrain());
                        grains.add(new BorderGrain());
                        grains.add(new BorderGrain());
                    } else if (j == space.y - 1) { //corner
                        if (space.fields[i - 1][j - 1].getParent().getPhase() == phase)
                            grains.add(space.fields[i - 1][j - 1].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[i - 1][j].getParent().getPhase() == phase)
                            grains.add(space.fields[i - 1][j].getParent());
                        else
                            grains.add(new VoidGrain());
                        grains.add(new BorderGrain());
                        grains.add(new BorderGrain());
                        grains.add(new BorderGrain());
                        grains.add(new BorderGrain());
                        grains.add(new BorderGrain());
                        if (space.fields[i][j - 1].getParent().getPhase() == phase)
                            grains.add(space.fields[i][j - 1].getParent());
                        else
                            grains.add(new VoidGrain());

                    } else { //just edge
                        if (space.fields[i - 1][j - 1].getParent().getPhase() == phase)
                            grains.add(space.fields[i - 1][j - 1].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[i - 1][j].getParent().getPhase() == phase)
                            grains.add(space.fields[i - 1][j].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[i - 1][j + 1].getParent().getPhase() == phase)
                            grains.add(space.fields[i - 1][j + 1].getParent());
                        else
                            grains.add(new VoidGrain());
                        if (space.fields[i][j + 1].getParent().getPhase() == phase)
                            grains.add(space.fields[i][j + 1].getParent());
                        else
                            grains.add(new VoidGrain());

                        grains.add(new BorderGrain());
                        grains.add(new BorderGrain());
                        grains.add(new BorderGrain());
                        if (space.fields[i][j - 1].getParent().getPhase() == phase)
                            grains.add(space.fields[i][j - 1].getParent());
                        else
                            grains.add(new VoidGrain());

                    }

                } else if (j == 0) {
                    grains.add(new BorderGrain());
                    if (space.fields[i - 1][j].getParent().getPhase() == phase)
                        grains.add(space.fields[i - 1][j].getParent());
                    else
                        grains.add(new VoidGrain());
                    if (space.fields[i - 1][j + 1].getParent().getPhase() == phase)
                        grains.add(space.fields[i - 1][j + 1].getParent());
                    else
                        grains.add(new VoidGrain());
                    if (space.fields[i][j + 1].getParent().getPhase() == phase)
                        grains.add(space.fields[i][j + 1].getParent());
                    else
                        grains.add(new VoidGrain());

                    if (space.fields[i + 1][j + 1].getParent().getPhase() == phase)
                        grains.add(space.fields[i + 1][j + 1].getParent());
                    else
                        grains.add(new VoidGrain());
                    if (space.fields[i + 1][j].getParent().getPhase() == phase)
                        grains.add(space.fields[i + 1][j].getParent());
                    else
                        grains.add(new VoidGrain());

                    grains.add(new BorderGrain());
                    grains.add(new BorderGrain());

                } else if (j == space.y - 1) {
                    if (space.fields[i - 1][j - 1].getParent().getPhase() == phase)
                        grains.add(space.fields[i - 1][j - 1].getParent());
                    else
                        grains.add(new VoidGrain());
                    if (space.fields[i - 1][j].getParent().getPhase() == phase)
                        grains.add(space.fields[i - 1][j].getParent());
                    else
                        grains.add(new VoidGrain());

                    grains.add(new BorderGrain());
                    grains.add(new BorderGrain());
                    grains.add(new BorderGrain());
                    if (space.fields[i + 1][j].getParent().getPhase() == phase)
                        grains.add(space.fields[i + 1][j].getParent());
                    else
                        grains.add(new VoidGrain());
                    if (space.fields[i + 1][j - 1].getParent().getPhase() == phase)
                        grains.add(space.fields[i + 1][j - 1].getParent());
                    else
                        grains.add(new VoidGrain());
                    if (space.fields[i][j - 1].getParent().getPhase() == phase)
                        grains.add(space.fields[i][j - 1].getParent());
                    else
                        grains.add(new VoidGrain());

                }
            }
        } else { //inside space
            if (space.fields[i - 1][j - 1].getParent().getPhase() == phase)
                grains.add(space.fields[i - 1][j - 1].getParent());
            else
                grains.add(new VoidGrain());
            if (space.fields[i - 1][j].getParent().getPhase() == phase)
                grains.add(space.fields[i - 1][j].getParent());
            else
                grains.add(new VoidGrain());
            if (space.fields[i - 1][j + 1].getParent().getPhase() == phase)
                grains.add(space.fields[i - 1][j + 1].getParent());
            else
                grains.add(new VoidGrain());
            if (space.fields[i][j + 1].getParent().getPhase() == phase)
                grains.add(space.fields[i][j + 1].getParent());
            else
                grains.add(new VoidGrain());
            if (space.fields[i + 1][j + 1].getParent().getPhase() == phase)
                grains.add(space.fields[i + 1][j + 1].getParent());
            else
                grains.add(new VoidGrain());
            if (space.fields[i + 1][j].getParent().getPhase() == phase)
                grains.add(space.fields[i + 1][j].getParent());
            else
                grains.add(new VoidGrain());
            if (space.fields[i + 1][j - 1].getParent().getPhase() == phase)
                grains.add(space.fields[i + 1][j - 1].getParent());
            else
                grains.add(new VoidGrain());
            if (space.fields[i][j - 1].getParent().getPhase() == phase)
                grains.add(space.fields[i][j - 1].getParent());
            else
                grains.add(new VoidGrain());

        }
        return grains;
    }
}
