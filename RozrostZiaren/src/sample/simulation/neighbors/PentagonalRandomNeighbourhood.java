package sample.simulation.neighbors;

import javafx.scene.paint.Color;
import sample.simulation.Space;
import sample.simulation.BoundaryConditions;
import sample.simulation.NeighbourhoodType;
import sample.simulation.grains.Grain;

import java.util.ArrayList;
import java.util.Random;

public class PentagonalRandomNeighbourhood extends PentagonalNeighbourhood {
    public PentagonalRandomNeighbourhood(NeighbourhoodType neighbourhoodType, BoundaryConditions boundaryConditions) {
        super(neighbourhoodType, boundaryConditions);
    }

    @Override
    @Deprecated
    public ArrayList<Color> getNeighborColors(Space space, int i, int j) {
        PentagonalNeighbourhood neighbourhood;
        Random random = new Random();
        int side=random.nextInt(4);
        switch(side){
            case 0:
                neighbourhood =  new PentagonalLeftNeighbourhood(neighbourhoodType,boundaryConditions);
                break;
            case 1:
                neighbourhood =  new PentagonalTopNeighbourhood(neighbourhoodType,boundaryConditions);
                break;
            case 2:
                neighbourhood =  new PentagonalRightNeighbourhood(neighbourhoodType,boundaryConditions);
                break;
            case 3:
                neighbourhood =  new PentagonalDownNeighbourhood(neighbourhoodType,boundaryConditions);
                break;
            default:
                neighbourhood =  new PentagonalLeftNeighbourhood(neighbourhoodType,boundaryConditions);
                break;
        }
        return neighbourhood.getNeighborColors(space,i,j);
    }

    @Override
    public ArrayList<Grain> getNeighborGrains(Space space, int i, int j, int phase){
        PentagonalNeighbourhood neighbourhood;
        Random random = new Random();
        int side=random.nextInt(4);
        switch(side){
            case 0:
                neighbourhood =  new PentagonalLeftNeighbourhood(neighbourhoodType,boundaryConditions);
                break;
            case 1:
                neighbourhood =  new PentagonalTopNeighbourhood(neighbourhoodType,boundaryConditions);
                break;
            case 2:
                neighbourhood =  new PentagonalRightNeighbourhood(neighbourhoodType,boundaryConditions);
                break;
            case 3:
                neighbourhood =  new PentagonalDownNeighbourhood(neighbourhoodType,boundaryConditions);
                break;
            default:
                neighbourhood =  new PentagonalLeftNeighbourhood(neighbourhoodType,boundaryConditions);
                break;
        }
        return neighbourhood.getNeighborGrains(space,i,j,phase);
    }
}
