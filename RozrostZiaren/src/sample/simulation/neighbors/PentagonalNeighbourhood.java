package sample.simulation.neighbors;

import javafx.scene.paint.Color;
import sample.MathUtils;
import sample.simulation.BoundaryConditions;
import sample.simulation.NeighbourhoodType;
import sample.simulation.Space;
import sample.simulation.grains.Grain;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public abstract class PentagonalNeighbourhood implements Neighbourhood {
    protected NeighbourhoodType neighbourhoodType;
    protected BoundaryConditions boundaryConditions;

    public PentagonalNeighbourhood(NeighbourhoodType neighbourhoodType, BoundaryConditions boundaryConditions) {
        this.neighbourhoodType = neighbourhoodType;
        this.boundaryConditions = boundaryConditions;
    }

    @Override
    @Deprecated
    public Color fieldColorCalc(Space space, int i, int j) {
        ArrayList<Color> colors = getNeighborColors(space, i, j);
        return checkFieldColor(colors);
    }

    @Override
    @Deprecated
    public Color checkFieldColor(ArrayList<Color> colors) {
        return checkFieldColor(colors, 1);
    }

    @Override
    @Deprecated
    public Color checkFieldColor(ArrayList<Color> colors, int threshold) {
        if ((colors.get(0) != Color.GRAY && colors.get(0) != Color.BLACK) || (colors.get(1) != Color.GRAY && colors.get(1) != Color.BLACK)
                || (colors.get(2) != Color.GRAY && colors.get(2) != Color.BLACK) || (colors.get(3) != Color.GRAY && colors.get(3) != Color.BLACK)
                || (colors.get(4) != Color.GRAY && colors.get(4) != Color.BLACK)) {


            int[] calc = {0, 0, 0, 0, 0, 0, 0, 0};

            if (colors.get(0) != Color.GRAY && colors.get(0) != Color.BLACK) {
                calc[0] = 1;
            }
            if (colors.get(1) != Color.GRAY && colors.get(1) != Color.BLACK) {
                //if same as previous
                if (colors.get(1) == colors.get(0)) {
                    calc[0]++;
                } else {
                    calc[1] = 1;
                }
            }
            if (colors.get(2) != Color.GRAY && colors.get(2) != Color.BLACK) {
                if (colors.get(2) == colors.get(0)) {
                    calc[0]++;
                } else if (colors.get(2) == colors.get(1)) {
                    calc[1]++;
                } else {
                    calc[2] = 1;
                }
            }
            if (colors.get(3) != Color.GRAY && colors.get(3) != Color.BLACK) {
                if (colors.get(3) == colors.get(0)) {
                    calc[0]++;
                } else if (colors.get(3) == colors.get(1)) {
                    calc[1]++;
                } else if (colors.get(3) == colors.get(2)) {
                    calc[2]++;
                } else {
                    calc[3] = 1;
                }
            }
            if (colors.get(4) != Color.GRAY && colors.get(4) != Color.BLACK) {
                if (colors.get(4) == colors.get(0)) {
                    calc[0]++;
                } else if (colors.get(4) == colors.get(1)) {
                    calc[1]++;
                } else if (colors.get(4) == colors.get(2)) {
                    calc[2]++;
                } else if (colors.get(4) == colors.get(3)) {
                    calc[3]++;
                } else {
                    calc[4] = 1;
                }
            }


            List<Integer> maxes = new ArrayList<>(); //create list for max values
            int maxIndex = MathUtils.getIndexOfLargest(calc);

            int max = MathUtils.getMaxt(calc);
            if (max >= threshold) { //check threshold
                //add max values Indexes
                if (calc[maxIndex] == calc[4] && colors.get(4) != Color.GRAY && colors.get(4) != Color.BLACK) {
                    maxes.add(4);
                }
                if (calc[maxIndex] == calc[3] && colors.get(3) != Color.GRAY && colors.get(3) != Color.BLACK) {
                    maxes.add(3);
                }
                if (calc[maxIndex] == calc[2] && colors.get(2) != Color.GRAY && colors.get(2) != Color.BLACK) {
                    maxes.add(2);
                }
                if (calc[maxIndex] == calc[1] && colors.get(1) != Color.GRAY && colors.get(1) != Color.BLACK) {
                    maxes.add(1);
                }
                if (calc[maxIndex] == calc[0] && colors.get(0) != Color.GRAY && colors.get(0) != Color.BLACK) {
                    maxes.add(0);
                }

                Random random = new Random();
                int searched = random.nextInt(maxes.size());

                // return chosen color
                switch (maxes.get(searched)) {
                    case 0:
                        return colors.get(0);
                    case 1:
                        return colors.get(1);
                    case 2:
                        return colors.get(2);
                    case 3:
                        return colors.get(3);
                    case 4:
                        return colors.get(4);
                }
            }
        }
        return null;
    }


    public Grain fieldGrainCalc(Space space, int i, int j, int phase) {
        ArrayList<Grain> grains = getNeighborGrains(space, i, j, phase);
        return checkFieldGrain(grains);
    }

    public Grain checkFieldGrain(ArrayList<Grain> grains) {
        return checkFieldGrain(grains, 1);
    }

    public Grain checkFieldGrain(ArrayList<Grain> grains, int threshold) {

        if ((grains.get(0).getColor() != Color.GRAY && grains.get(0).getColor() != Color.BLACK)
                || (grains.get(1).getColor() != Color.GRAY && grains.get(1).getColor() != Color.BLACK)
                || (grains.get(2).getColor() != Color.GRAY && grains.get(2).getColor() != Color.BLACK)
                || (grains.get(3).getColor() != Color.GRAY && grains.get(3).getColor() != Color.BLACK)
                || (grains.get(4).getColor() != Color.GRAY && grains.get(4).getColor() != Color.BLACK)) {


            int[] calc = {0, 0, 0, 0, 0, 0, 0, 0};

            if (grains.get(0).getColor() != Color.GRAY && grains.get(0).getColor() != Color.BLACK) {
                calc[0] = 1;
            }
            if (grains.get(1).getColor() != Color.GRAY && grains.get(1).getColor() != Color.BLACK) {
                if (grains.get(1).getColor() == grains.get(0).getColor()) {
                    calc[0]++;
                } else {
                    calc[1] = 1;
                }
            }
            if (grains.get(2).getColor() != Color.GRAY && grains.get(2).getColor() != Color.BLACK) {
                if (grains.get(2).getColor() == grains.get(0).getColor()) {
                    calc[0]++;
                } else if (grains.get(2).getColor() == grains.get(1).getColor()) {
                    calc[1]++;
                } else {
                    calc[2] = 1;
                }
            }
            if (grains.get(3).getColor() != Color.GRAY && grains.get(3).getColor() != Color.BLACK) {
                if (grains.get(3).getColor() == grains.get(0).getColor()) {
                    calc[0]++;
                } else if (grains.get(3).getColor() == grains.get(1).getColor()) {
                    calc[1]++;
                } else if (grains.get(3).getColor() == grains.get(2).getColor()) {
                    calc[2]++;
                } else {
                    calc[3] = 1;
                }
            }
            if (grains.get(4).getColor() != Color.GRAY && grains.get(4).getColor() != Color.BLACK) {
                if (grains.get(4).getColor() == grains.get(0).getColor()) {
                    calc[0]++;
                } else if (grains.get(4).getColor() == grains.get(1).getColor()) {
                    calc[1]++;
                } else if (grains.get(4).getColor() == grains.get(2).getColor()) {
                    calc[2]++;
                } else if (grains.get(4).getColor() == grains.get(3).getColor()) {
                    calc[3]++;
                } else {
                    calc[4] = 1;
                }
            }


            List<Integer> maxes = new ArrayList<>();
            int maxIndex = MathUtils.getIndexOfLargest(calc);

            int max = MathUtils.getMaxt(calc);
            if (max >= threshold) {

                if (calc[maxIndex] == calc[4] && grains.get(4).getColor() != Color.GRAY && grains.get(4).getColor() != Color.BLACK) {
                    maxes.add(4);
                }
                if (calc[maxIndex] == calc[3] && grains.get(3).getColor() != Color.GRAY && grains.get(3).getColor() != Color.BLACK) {
                    maxes.add(3);
                }
                if (calc[maxIndex] == calc[2] && grains.get(2).getColor() != Color.GRAY && grains.get(2).getColor() != Color.BLACK) {
                    maxes.add(2);
                }
                if (calc[maxIndex] == calc[1] && grains.get(1).getColor() != Color.GRAY && grains.get(1).getColor() != Color.BLACK) {
                    maxes.add(1);
                }
                if (calc[maxIndex] == calc[0] && grains.get(0).getColor() != Color.GRAY && grains.get(0).getColor() != Color.BLACK) {
                    maxes.add(0);
                }

                Random random = new Random();
                int searched = random.nextInt(maxes.size());


                switch (maxes.get(searched)) {
                    case 0:
                        return grains.get(0);
                    case 1:
                        return grains.get(1);
                    case 2:
                        return grains.get(2);
                    case 3:
                        return grains.get(3);
                    case 4:
                        return grains.get(4);
                }
            }
        }
        return null;

    }

}
