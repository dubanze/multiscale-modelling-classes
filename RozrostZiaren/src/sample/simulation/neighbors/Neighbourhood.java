package sample.simulation.neighbors;

import javafx.scene.paint.Color;
import sample.simulation.Space;
import sample.simulation.grains.Grain;

import java.util.ArrayList;

public interface Neighbourhood {

    @Deprecated
    Color fieldColorCalc(Space space, int i, int j);

    @Deprecated
    Color checkFieldColor(ArrayList<Color> colors);

    @Deprecated
    Color checkFieldColor(ArrayList<Color> colors, int threshold);

    @Deprecated
    ArrayList<Color> getNeighborColors(Space space, int i, int j);


    Grain fieldGrainCalc(Space space, int i, int j, int phase);

    Grain checkFieldGrain(ArrayList<Grain> grains);

    Grain checkFieldGrain(ArrayList<Grain> grains, int threshold);

    ArrayList<Grain> getNeighborGrains(Space space, int i, int j, int phase);

}
