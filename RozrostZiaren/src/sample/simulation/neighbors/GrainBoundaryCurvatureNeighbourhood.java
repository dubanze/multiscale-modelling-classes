package sample.simulation.neighbors;

import javafx.scene.paint.Color;
import sample.simulation.BoundaryConditions;
import sample.simulation.NeighbourhoodType;
import sample.simulation.Space;
import sample.simulation.grains.Grain;

import java.util.ArrayList;
import java.util.Random;

public class GrainBoundaryCurvatureNeighbourhood implements Neighbourhood {
    private NeighbourhoodType neighbourhoodType;
    private BoundaryConditions boundaryConditions;
    private double threshold;

    public GrainBoundaryCurvatureNeighbourhood(NeighbourhoodType neighbourhoodType, BoundaryConditions boundaryConditions, double threshold) {
        this.neighbourhoodType = neighbourhoodType;
        this.boundaryConditions = boundaryConditions;
        this.threshold = threshold;
    }

    @Override
    @Deprecated
    public Color fieldColorCalc(Space space, int i, int j) {

        Color color;
        Neighbourhood neighbourhood = new MooreNeighbourhood(neighbourhoodType, boundaryConditions);
        ArrayList<Color> colors = neighbourhood.getNeighborColors(space, i, j);
        color = neighbourhood.checkFieldColor(colors, 5);
        if (color != null) {
            return color;
        } else {


            neighbourhood = new VonNeumannNeighbourhood(neighbourhoodType, boundaryConditions);
            colors = neighbourhood.getNeighborColors(space, i, j);
            color = neighbourhood.checkFieldColor(colors, 3);
            if (color != null) {
                return color;
            } else {


                neighbourhood = new MooreFurtherNeighbourhood(neighbourhoodType, boundaryConditions);
                colors = neighbourhood.getNeighborColors(space, i, j);
                color = neighbourhood.checkFieldColor(colors, 3);
                if (color != null) {
                    return color;
                } else {

                    Random random = new Random();
                    double r = random.nextDouble();
                    if (r <= threshold) {
                        neighbourhood = new MooreNeighbourhood(neighbourhoodType, boundaryConditions);
                        color = neighbourhood.fieldColorCalc(space, i, j);
                        {
                            return color;
                        }
                    } else {
                        return null;
                    }
                }
            }
        }

    }

    @Override
    @Deprecated
    public Color checkFieldColor(ArrayList<Color> colors) {
        return null;
    }

    @Override
    @Deprecated
    public Color checkFieldColor(ArrayList<Color> colors, int threshold) {
        return null;
    }

    @Override
    @Deprecated
    public ArrayList<Color> getNeighborColors(Space space, int i, int j) {
        return null;
    }

    @Override
    public Grain fieldGrainCalc(Space space, int i, int j, int phase) {
        Grain grain;
        //Rule 1
        Neighbourhood neighbourhood = new MooreNeighbourhood(neighbourhoodType, boundaryConditions);
        ArrayList<Grain> grains = neighbourhood.getNeighborGrains(space, i, j, phase);
        grain = neighbourhood.checkFieldGrain(grains, 5);
        if (grain != null) {
            //System.out.println("Rule 1");
            return grain;
        } else {


            //Rule 2
            neighbourhood = new VonNeumannNeighbourhood(neighbourhoodType, boundaryConditions);
            grains = neighbourhood.getNeighborGrains(space, i, j, phase);
            grain = neighbourhood.checkFieldGrain(grains, 3);
            if (grain != null) {
                //System.out.println("Rule 2");
                return grain;
            } else {


                //Rule 3
                neighbourhood = new MooreFurtherNeighbourhood(neighbourhoodType, boundaryConditions);
                grains = neighbourhood.getNeighborGrains(space, i, j, phase);
                grain = neighbourhood.checkFieldGrain(grains, 3);
                if (grain != null) {         //System.out.println("Rule 3");
                    return grain;
                } else {

                    //Rule 4
                    Random random = new Random();
                    double r = random.nextDouble();
                    if (r <= threshold) {
                        neighbourhood = new MooreNeighbourhood(neighbourhoodType, boundaryConditions);
                        grain = neighbourhood.fieldGrainCalc(space, i, j, phase);
                        {
                            //System.out.println("Rule 4:" +r);
                            return grain;
                        }
                    } else {
                        //System.out.println("Threshold not passed!");
                        return null;
                    }
                }
            }
        }
    }


    @Override
    public Grain checkFieldGrain(ArrayList<Grain> grains) {
        return null;
    }

    @Override
    public Grain checkFieldGrain(ArrayList<Grain> grains, int threshold) {
        return null;
    }

    @Override
    public ArrayList<Grain> getNeighborGrains(Space space, int i, int j, int phase) {
        return null;
    }


}
