package sample.simulation;

import javafx.scene.image.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import sample.simulation.grains.Field;
import sample.simulation.grains.Grain;
import sample.simulation.neighbors.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Space implements Cloneable {

    public int x, y, scale;

    public BoundaryConditions boundaryConditions;
    private int size;

    public Field fields[][];
    public ArrayList<Grain> grains = new ArrayList<>();
    ;
    public ArrayList<Paint> alive = new ArrayList<>();
    private WritableImage image;


    private ArrayList<Paint> reservedColors;


    public Space(int x, int y, BoundaryConditions boundaryConditions) {
        //this.controller = controller;
        this.boundaryConditions = boundaryConditions;
        this.x = x;
        this.y = y;
        scale = 1;
        image = new WritableImage(x, y);
        grains = new ArrayList<>();

        reservedColors = new ArrayList<>();
        reservedColors.add(Color.BLACK);
        reservedColors.add(Color.GRAY);
        reservedColors.add(Color.WHITE);

    }

    public void autoBalanceColors() { //TODO KAIZEN color balancing inside grains - neighbors should be different from themselves


    }

    public void add(Grain grain) {
        this.grains.add(grain);
    }

    public synchronized void updateImage(List<Field> toUpdate) {
        for (Field f : toUpdate) {
            image.getPixelWriter().setColor(f.getX(), f.getY(), f.getColor());
        }
    }

    public synchronized void replaceImage(WritableImage toUpdate) {
        this.image = toUpdate;
    }

    public synchronized WritableImage returnImage() {
        return this.image;
    }

    public synchronized Object clone() throws CloneNotSupportedException {
        Space cloned = new Space(this.x, this.y, this.boundaryConditions);
        cloned.fields = new Field[x][y];
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                cloned.fields[i][j] = (Field) fields[i][j].clone();
                if (fields[i][j].getParent() != null) {
                    boolean toAdd = true;
                    for (Grain g : cloned.grains) {
                        if (g.equals(fields[i][j].getParent())) {
                            toAdd = false;
                        }
                    }
                    if (toAdd) {
                        Grain g = (Grain) fields[i][j].getParent().clone();
                        cloned.grains.add(g);
                        cloned.fields[i][j].setParent(g);

                    }
                }
            }
        }
        cloned.generate();
        return cloned;
    }

    public synchronized AnchorPane returnImagePane() {
        AnchorPane ret = new AnchorPane();
        if (x > y) {
            size = x;
            //size = (int) scale * 700 / y;
        } else {
            size = y;
            // size = (int) scale * 700 / x;
        }
        ImageView imageView;
        if (x < 700 && y < 700) {
            imageView = new ImageView(resample(image, 700 / size));
        } else {
            imageView = new ImageView(image);
        }


        ret.getChildren().add(imageView);
        imageView.fitHeightProperty().bind(ret.heightProperty());
        imageView.fitWidthProperty().bind(ret.widthProperty());
        return ret;
    }


    public synchronized AnchorPane generate() {
        AnchorPane ret = new AnchorPane();
        if (x > y) {
            size = x;
        } else {
            size = y;
        }
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                image.getPixelWriter().setColor(i, j, fields[i][j].getColor());
            }
        }

        ImageView imageView;
        if (x < 700 && y < 700) {
            scale = 700 / size;
            imageView = new ImageView(resample(image, scale));
        } else {
            imageView = new ImageView(image);
        }
        ret.getChildren().add(imageView);
        imageView.fitHeightProperty().bind(ret.heightProperty());
        imageView.fitWidthProperty().bind(ret.widthProperty());
        return ret;
    }

    public synchronized boolean isPlace() {
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                if (fields[i][j].isEmpty()) return true;
            }
        }
        return false;
    }

    public synchronized Color nextPaint() {
        boolean generated = false;
        Random rand = new Random();

        Color randomColor = null;
        while (!generated) {

            int r = rand.nextInt(256);
            int g = rand.nextInt(256);
            int b = rand.nextInt(256);


            if (r == g && g == b) {
                generated = false;
            } else {
                randomColor = Color.rgb(r, g, b, 1.0);
                if (randomColor == Color.GRAY || randomColor == Color.BLACK) {
                    generated = false;
                } else if (alive.isEmpty()) {
                    generated = true;
                } else {
                    boolean isused = false;
                    for (Paint anUsed : alive) {
                        if (randomColor == anUsed) {
                            isused = true;
                        }
                    }
                    if (!isused)
                        generated = true;
                }
            }
        }
        alive.add(randomColor);
        return randomColor;

    }


    public synchronized List<List<String>> dumpExport() { //TODO rewrite
        List<List<String>> dumpFields = new ArrayList<>();
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                dumpFields.add(fields[i][j].dumpExport());
            }
        }
        return dumpFields;
    }

    public synchronized void checkAlive() { //TODO is this needed?
        ArrayList<Paint> a = new ArrayList<>();

        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                if (!a.contains(fields[i][j].getColor())) {
                    a.add(fields[i][j].getColor());
                }
            }
        }

        alive = a;
    }

    public void synchoGrains() {
        ArrayList<Grain> newGrains = new ArrayList<>();
        for (Grain g : grains
        ) {
            if (newGrains.isEmpty()) {
                newGrains.add(g);
            } else {
                boolean ishere = false;
                for (Grain nG : newGrains) {
                    if (g.getColor().equals(nG.getColor())) {
                        for (Field f : g.getFields()) {
                            nG.add(f);
                        }
                        ishere = true;
                    }
                }
                if (!ishere) {
                    newGrains.add(g);
                }
            }


        }


        grains = newGrains;
    }

    public void addAlive(Color color) {
        alive.add(color);
    }

    private synchronized WritableImage resample(Image input, int scaleFactor) {
        final int W = (int) input.getWidth();
        final int H = (int) input.getHeight();
        final int S = scaleFactor;

        WritableImage output = new WritableImage(
                W * S,
                H * S
        );

        PixelReader reader = input.getPixelReader();
        PixelWriter writer = output.getPixelWriter();

        for (int y = 0; y < H; y++) {
            for (int x = 0; x < W; x++) {
                final int argb = reader.getArgb(x, y);
                for (int dy = 0; dy < S; dy++) {
                    for (int dx = 0; dx < S; dx++) {
                        writer.setArgb(x * S + dx, y * S + dy, argb);
                    }
                }
            }
        }

        return output;
    }

    public ArrayList<Field> detectBoundaties(NeighbourhoodType neighbourhoodType, BoundaryConditions boundaryConditions) {
        ArrayList<Field> boundaries = new ArrayList<>();
        Neighbourhood neighbourhood = null;
        switch (neighbourhoodType) {
            case VonNeumann:
                neighbourhood = new VonNeumannNeighbourhood(neighbourhoodType, boundaryConditions);
                break;
            case Moore:
                neighbourhood = new MooreNeighbourhood(neighbourhoodType, boundaryConditions);
                break;
            case HexagonalLeft:
                neighbourhood = new HexagonalLeftNeighbourhood(neighbourhoodType, boundaryConditions);
                break;
            case HexagonalRight:
                neighbourhood = new HexagonalRightNeighbourhood(neighbourhoodType, boundaryConditions);
                break;
            case HexagonalRandom:
                neighbourhood = new HexagonalRandomNeighbourhood(neighbourhoodType, boundaryConditions);
                break;
            case PentagonalRight:
                neighbourhood = new PentagonalRightNeighbourhood(neighbourhoodType, boundaryConditions);
                break;
            case PentagonalDown:
                neighbourhood = new PentagonalDownNeighbourhood(neighbourhoodType, boundaryConditions);
                break;
            case PentagonalLeft:
                neighbourhood = new PentagonalLeftNeighbourhood(neighbourhoodType, boundaryConditions);
                break;
            case PentagonalTop:
                neighbourhood = new PentagonalTopNeighbourhood(neighbourhoodType, boundaryConditions);
                break;
            case PentagonalRandom:
                neighbourhood = new PentagonalRandomNeighbourhood(neighbourhoodType, boundaryConditions);
                break;
            case GrainBoundaryCurvature:
                System.out.println("Unable to check");
                break;
            default:
                neighbourhood = new VonNeumannNeighbourhood(neighbourhoodType, boundaryConditions);
                break;

        }
        ArrayList<Color> neighbours;
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {

                neighbours = neighbourhood.getNeighborColors(this, i, j);
                for (Color n : neighbours) {
                    if (n != fields[i][j].getColor()) {
                        boundaries.add(fields[i][j]);
                    }
                }
            }
        }
        return boundaries;
    }

    public void clearBoundaries() {
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                fields[i][j].setBoundary(false);
            }
        }
    }

    public int getGrainsCount() {
        alive.clear();
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                boolean isAdded = false;
                for (Paint c : alive) {
                    if (fields[i][j].getColor().equals(c) && !fields[i][j].getColor().equals(Color.GRAY)) {
                        isAdded = true;
                    }
                }
                if (!isAdded) {
                    alive.add(fields[i][j].getColor());
                }
            }
        }
        return alive.size();
    }


}