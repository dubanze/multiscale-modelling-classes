package sample.simulation.grains;

import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.List;

public class Grain implements Cloneable {

    public ArrayList<Field> getFields() {
        return fields;
    }

    ArrayList<Field> fields=new ArrayList<>();
    MaterialType materialType;
    int phase;
    protected Color color;

    public Grain(MaterialType materialType, int phase){
        this.materialType=materialType;
        this.phase=phase;
    }

    public Grain(MaterialType materialType, int phase, Color color){
        this.materialType=materialType;
        this.phase=phase;
        this.color=color;
    }

    public void add(Field field){
        field.setParent(this);
        this.fields.add(field);
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color){
        this.color=color;
    }
    public MaterialType getMaterialType(){
        return this.materialType;
    }

    public synchronized Object clone() throws CloneNotSupportedException {
        Grain cloned = new Grain(this.materialType,this.phase);
        cloned.setColor(this.getColor());
        for(Field f:this.fields){
            Field clonedField=(Field)f.clone();
            clonedField.setParent(cloned);
            cloned.fields.add(clonedField);
        }
        return cloned;
    }

    public int getPhase(){
        return phase;
    }

    public List<Field> delete(){
        List<Field> toUpdate= new ArrayList<>();
        for (Field f: this.fields
        ) {
            f.setParent(null);
            toUpdate.add(f);
        }
        this.fields.clear();
        return toUpdate;

    }

    public void setMaterialType(MaterialType materialType) {
        this.materialType=materialType;
    }

    public void export(){}//TODO LAST add export
}
