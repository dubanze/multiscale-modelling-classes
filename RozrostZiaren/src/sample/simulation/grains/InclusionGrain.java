package sample.simulation.grains;

import javafx.scene.paint.Color;

public class InclusionGrain extends Grain {
    double center_x, center_y, radius;


    public InclusionGrain(MaterialType materialType, int phase, double x, double y, double r) {
        super(materialType, phase);
        this.color = Color.BLACK;
        this.center_x = x;
        this.center_y = y;
        this.radius = r;
    }

    public double getX() {
        return center_x;
    }

    public double getY() {
        return center_y;
    }

    public double getRadius() {
        return radius;
    }
}
