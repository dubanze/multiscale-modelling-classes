package sample.simulation.grains;

import javafx.scene.paint.Color;

public class VoidGrain extends Grain {
    public VoidGrain(MaterialType materialType, int phase) {
        super(materialType, phase);
        this.color = Color.GRAY;
    }

    public VoidGrain() {
        super(MaterialType.VOID, 0);

        this.color = Color.GRAY;
    }
}
