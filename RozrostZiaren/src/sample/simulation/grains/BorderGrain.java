package sample.simulation.grains;

import javafx.scene.paint.Color;

public class BorderGrain extends Grain {
    public BorderGrain(MaterialType materialType, int phase) {
        super(materialType, phase);
        this.color = Color.BLACK;
    }

    public BorderGrain() {
        super(MaterialType.VOID, 0);

        this.color = Color.BLACK;
    }
}
