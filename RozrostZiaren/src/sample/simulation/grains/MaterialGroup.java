package sample.simulation.grains;

public enum MaterialGroup {
    METAL, NONMETAL,  // Elements
    ALLOY, COMPOUND, MIXTURE, //compounds, mixtures and alloys
    VOID
}