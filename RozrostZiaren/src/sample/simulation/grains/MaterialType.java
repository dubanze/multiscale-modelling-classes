package sample.simulation.grains;

public enum MaterialType {
    AUSTENITE("AUSTENITE"),
    CARBON_DIOXIDE("CARBON_DIOXIDE"),
    COAL("COAL"),
    FERRITE("FERRITE"),
    IRON("IRON"),
    MARTENSITE("MARTENSITE"),
    OXYGEN("OXYGEN"),
    PERLITE("PERLITE"),
    STEEL("STEEL"),
    VOID("VOID"),
    ;

    private MaterialGroup groupAffilation;
    private String name;

    public boolean isMetal() {
        if (groupAffilation == MaterialGroup.METAL)
            return true;
        else return false;
    }

    MaterialType(String name) {
        this.name = name;
        switch (name) {

            case "STEEL":
                this.groupAffilation = MaterialGroup.ALLOY;
                break;
            case "IRON":
                this.groupAffilation = MaterialGroup.METAL;
                break;
            case "CARBON_DIOXIDE":
                this.groupAffilation = MaterialGroup.COMPOUND;
                break;
            case "OXYGEN":
                this.groupAffilation = MaterialGroup.NONMETAL;
                break;
            case "AUSTENITE":
                this.groupAffilation = MaterialGroup.MIXTURE;
                break;
            case "PERLITE":
                this.groupAffilation = MaterialGroup.MIXTURE;
                break;
            case "FERRITE":
                this.groupAffilation = MaterialGroup.MIXTURE;
                break;
            case "MARTENSITE":
                this.groupAffilation = MaterialGroup.MIXTURE;
                break;
            case "VOID":
                this.groupAffilation = MaterialGroup.VOID;
                break;
        }
    }

    public MaterialGroup getGroupAffilation() {
        return groupAffilation;
    }


}
