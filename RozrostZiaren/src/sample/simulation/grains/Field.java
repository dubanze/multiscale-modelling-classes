package sample.simulation.grains;

import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.List;


public class Field implements Cloneable {
    private int x;
    private int y;
    private boolean isBoundary;
    private Grain parent;


    @Override
    public String toString() {

        return "Field { x: " + x + "; y: " + y + "; Color:" + getColor() + ";";
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Field(int x, int y, Grain parent) {
        this.x = x;
        this.y = y;
        this.parent = parent;
        this.isBoundary = false;
    }

    public Field(int x, int y) {
        this.x = x;
        this.y = y;
        this.parent = null;
        this.isBoundary = false;
    }


    public List<String> dumpExport() {

        List<String> thisField = new ArrayList<>();
        thisField.add(Integer.toString(x));
        thisField.add(Integer.toString(y));
        if (parent != null) thisField.add(parent.getColor().toString());
        else thisField.add(Color.GRAY.toString());
        thisField.add(Integer.toString(getPhase()));
        if (isBoundary) thisField.add("true");
        else thisField.add("false");
        return thisField;
    }


    public boolean isEmpty() {
        if (this.parent == null)
            return true;
        else
            return false;
    }


    public void setParent(Grain grain) {
        this.parent = grain;
    }

    public Grain getParent() {
        return parent;
    }

    public Color getColor() {
        if (this.parent == null)
            return Color.GRAY;
        else if (this.isBoundary)
            return Color.BLACK;
        else
            return parent.getColor();
    }

    public synchronized Object clone() throws CloneNotSupportedException {
        Field cloned = new Field(this.x, this.y);
        return cloned;
    }

    public int getPhase() {
        if (parent != null) {
            return parent.phase;
        } else
            return 0;
    }

    public void setBoundary(boolean b) {
        this.isBoundary = b;
    }
}
