package sample;

import javafx.beans.property.BooleanPropertyBase;
import javafx.beans.property.Property;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.*;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import sample.simulation.BoundaryConditions;
import sample.simulation.NeighbourhoodType;
import sample.simulation.Seeding;
import sample.simulation.Space;
import sample.simulation.grains.Field;
import sample.simulation.grains.Grain;
import sample.simulation.grains.MaterialType;
import sample.tasks.*;

import javax.imageio.ImageIO;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Controller {

    public Space space;
    private Grain selectedGrain;

    public int boardX;
    public int boardY;

    public Seeding seedingType;
    public int seedCount;

    int phaseVal;

    public NeighbourhoodType neighbourhoodType;
    public BoundaryConditions boundaryConditions;
    public double grainThreshold;

    int stepsGenerationIndicator;
    boolean play;

    Property<Boolean> destroyerMode;

    double totalBoundariesLength;
    double meanGrainArea;

    @FXML
    protected AnchorPane output;

    @FXML
    protected Text grainInfo;
    @FXML
    private TextField inputX;
    @FXML
    private TextField inputY;

    @FXML
    private Button generateSpaceButton;
    @FXML
    private Button resetSpaceButton;

    @FXML
    private RadioButton randomSeedRadio;
    @FXML
    private RadioButton regularSeedRadio;
    @FXML
    private TextField inputSeedVal;
    @FXML
    private Button generateSeedButton;


    boolean grainCurvature;
    @FXML
    private RadioButton grainCurvatureOn;
    @FXML
    private RadioButton grainCurvatureOff;
    @FXML
    private TextField grainCurvatureRuleThreshold;

    @FXML
    private RadioButton periodicRadio;
    @FXML
    private RadioButton nonperiodicRadio;

    @FXML
    private RadioButton vonNeumannRadio;
    @FXML
    private RadioButton mooreRadio;
    @FXML
    private RadioButton hexagonalRightRadio;
    @FXML
    private RadioButton hexagonalLeftRadio;
    @FXML
    private RadioButton hexagonalRandomRadio;
    @FXML
    private RadioButton pentagonalRightRadio;
    @FXML
    private RadioButton pentagonalLeftRadio;
    @FXML
    private RadioButton pentagonalRandomRadio;


    @FXML
    Text phaseText;
    @FXML
    Button nextPhaseButton;
    @FXML
    Button deleteGrainButton;
    @FXML
    ToggleButton destroyerModeButton;

    @FXML
    private TextField inclusionAmountField;
    @FXML
    private TextField inclusionMinRadiusField;
    @FXML
    private TextField inclusionMaxRadiusField;
    @FXML
    private Button inclusionGenerateButton;


    @FXML
    private Button playPauseButton;
    @FXML
    private TextField stepsVal;
    @FXML
    private Button renderStepsButton;
    @FXML
    private ProgressBar stepProgressBar;


    @FXML
    private Button imageExportButton;
    @FXML
    private Button dumpExportButton;
    @FXML
    private Button dumpImportButton;


    @FXML
    private Button boundariesDetectionButton;

    @FXML
    private Text meanGrainAreaText;
    @FXML
    private Text totalBoundariesLenghtText;


    @FXML
    private void handleDetectBoundariesAndCalculateStatistics() {
        System.out.println("Size of alive array: " + space.alive.size());
        System.out.println("Size of grains array: " + space.grains.size());
        System.out.println("Size of alive count: " + space.getGrainsCount());
        lockGUI();
        space.clearBoundaries();

        ArrayList<Field> boundaries = space.detectBoundaties(this.neighbourhoodType, this.boundaryConditions);
        totalBoundariesLength = boundaries.size() / 2.0;
        meanGrainArea = (double) (space.x * space.y * 1.0f / space.alive.size());
        totalBoundariesLenghtText.setText(String.valueOf(totalBoundariesLength));
        meanGrainAreaText.setText(String.valueOf(meanGrainArea));
        for (Field f : boundaries) {
            f.setBoundary(true);
        }
        updateOutput(space.generate(), space, true);
        unlockGUI();
    }


    @FXML
    public void handleGrainSelect(MouseEvent event) {

        if (destroyerMode.getValue()) {
            int x = (int) event.getX() / space.scale;
            int y = (int) event.getY() / space.scale;
            Grain parent = space.fields[x][y].getParent();
            selectedGrain = parent;
            handleDeleteGrain();
        } else {
            int x = (int) event.getX() / space.scale;
            int y = (int) event.getY() / space.scale;
            Grain parent = space.fields[x][y].getParent();
            selectedGrain = parent;
            if (parent != null) {

                grainInfo.setText("Grain selected!");
                //grainInfo.setText("x:" + x + " y:" + y + " - there is Grain here, Color:" + parent.getColor().toString() + "; Material: " + parent.getMaterialType() + " phase:" + parent.getPhase());

            } else {

                grainInfo.setText("There is no grain present in selected place!");
                //grainInfo.setText("x:" + x + " y:" + y + " - there is noting here now");
            }
        }

    }

    public void lockGUI() {
        inputX.setDisable(true);
        inputY.setDisable(true);
        generateSpaceButton.setDisable(true);
        resetSpaceButton.setDisable(true);
        randomSeedRadio.setDisable(true);
        regularSeedRadio.setDisable(true);
        inputSeedVal.setDisable(true);
        generateSeedButton.setDisable(true);

        grainCurvatureOff.setDisable(true);
        grainCurvatureOn.setDisable(true);
        grainCurvatureRuleThreshold.setDisable(true);
        periodicRadio.setDisable(true);
        nonperiodicRadio.setDisable(true);
        vonNeumannRadio.setDisable(true);
        mooreRadio.setDisable(true);
        hexagonalRightRadio.setDisable(true);
        hexagonalLeftRadio.setDisable(true);
        hexagonalRandomRadio.setDisable(true);
        pentagonalRightRadio.setDisable(true);
        pentagonalLeftRadio.setDisable(true);
        pentagonalRandomRadio.setDisable(true);


        stepsVal.setDisable(true);
        renderStepsButton.setDisable(true);
        inclusionAmountField.setDisable(true);
        inclusionMaxRadiusField.setDisable(true);
        inclusionMinRadiusField.setDisable(true);
        inclusionGenerateButton.setDisable(true);
    }

    public void unlockGUI() {
        inputX.setDisable(false);
        inputY.setDisable(false);
        generateSpaceButton.setDisable(false);
        resetSpaceButton.setDisable(false);
        randomSeedRadio.setDisable(false);
        regularSeedRadio.setDisable(false);
        inputSeedVal.setDisable(false);
        generateSeedButton.setDisable(false);

        grainCurvatureOff.setDisable(false);
        grainCurvatureOn.setDisable(false);
        grainCurvatureRuleThreshold.setDisable(false);
        periodicRadio.setDisable(false);
        nonperiodicRadio.setDisable(false);

        if (!grainCurvature) {
            vonNeumannRadio.setDisable(false);
            mooreRadio.setDisable(false);
            hexagonalRightRadio.setDisable(false);
            hexagonalLeftRadio.setDisable(false);
            hexagonalRandomRadio.setDisable(false);
            pentagonalRightRadio.setDisable(false);
            pentagonalLeftRadio.setDisable(false);
            pentagonalRandomRadio.setDisable(false);
        }

        stepsVal.setDisable(false);
        renderStepsButton.setDisable(false);
        inclusionAmountField.setDisable(false);
        inclusionMaxRadiusField.setDisable(false);
        inclusionMinRadiusField.setDisable(false);
        inclusionGenerateButton.setDisable(false);

    }


    public Controller() {

    }

    @FXML
    private void initialize() {

        seedingType = Seeding.Random;
        randomSeedRadio.setSelected(true);

        neighbourhoodType = NeighbourhoodType.VonNeumann;
        vonNeumannRadio.setSelected(true);

        boundaryConditions = BoundaryConditions.Periodic;
        periodicRadio.setSelected(true);

        grainCurvature = false;
        grainCurvatureOff.setSelected(true);
        play = false;
        phaseVal = 1;
        destroyerMode = new BooleanPropertyBase() {
            @Override
            public Object getBean() {
                return null;
            }

            @Override
            public String getName() {
                return null;
            }
        };
        destroyerModeButton.selectedProperty().bindBidirectional(destroyerMode);

        destroyerMode.setValue(false);
    }

    @FXML
    private void handleRandom() {
        seedingType = Seeding.Random;
        randomSeedRadio.setSelected(true);

        regularSeedRadio.setSelected(false);

    }

    @FXML
    private void handleRegular() {
        seedingType = Seeding.Evenly;
        randomSeedRadio.setSelected(false);
        regularSeedRadio.setSelected(true);
    }

    @FXML
    private void handleGenerate() {
        lockGUI();
        boardX = Integer.parseInt(inputX.getText());
        boardY = Integer.parseInt(inputY.getText());
        output.setPrefSize(boardX, boardY);
        GenerateSpaceTask generateSpaceTask = new GenerateSpaceTask(boardX, boardY, this);
        stepProgressBar.progressProperty().bind(generateSpaceTask.progressProperty());
        try {
            ExecutorService executorService
                    = Executors.newFixedThreadPool(1);
            executorService.execute(generateSpaceTask);
            executorService.shutdown();
        } catch (Exception e) {
            e.printStackTrace();
        }
        phaseVal = 1;
        phaseText.setText("Phase " + phaseVal);

    }


    @FXML
    private void handleReset() {
        lockGUI();
        GenerateSpaceTask generateSpaceTask = new GenerateSpaceTask(space.x, space.y, this);
        stepProgressBar.progressProperty().bind(generateSpaceTask.progressProperty());
        try {
            ExecutorService executorService
                    = Executors.newFixedThreadPool(1);
            executorService.execute(generateSpaceTask);
            executorService.shutdown();
            executorService.awaitTermination(60, TimeUnit.SECONDS);
        } catch (Exception e) {
            e.printStackTrace();
        }
        phaseVal = 1;
        phaseText.setText("Phase " + phaseVal);
    }

    @FXML
    private void handleSpawn() {
        lockGUI();
        seedCount = Integer.parseInt(inputSeedVal.getText());

        GenerateSeedsTask generateSeedsTask = new GenerateSeedsTask(space, this, this.seedCount, this.phaseVal);
        stepProgressBar.progressProperty().bind(generateSeedsTask.progressProperty());
        try {
            ExecutorService executorService
                    = Executors.newFixedThreadPool(1);
            executorService.execute(generateSeedsTask);
            executorService.shutdown();
            executorService.awaitTermination(60, TimeUnit.SECONDS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void handleSeedCount() {
        seedCount = Integer.parseInt(inputSeedVal.getText());
    }

    @FXML
    private void handleGenerateInclusions() {
        lockGUI();
        int amount = Integer.parseInt(inclusionAmountField.getText());
        double min = Double.parseDouble(inclusionMinRadiusField.getText());
        double max = Double.parseDouble(inclusionMaxRadiusField.getText());

        GenerateInclusionsTask generateInclusionsTask = new GenerateInclusionsTask(space, this, amount, min, max);
        stepProgressBar.progressProperty().bind(generateInclusionsTask.progressProperty());
        try {
            ExecutorService executorService
                    = Executors.newFixedThreadPool(1);
            executorService.execute(generateInclusionsTask);
            executorService.shutdown();
            executorService.awaitTermination(60, TimeUnit.SECONDS);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @FXML
    public void handleGBCOn() {
        deselectNeighbourhoods();
        mooreRadio.setSelected(true);
        grainCurvature = true;
        neighbourhoodType = NeighbourhoodType.GrainBoundaryCurvature;
        grainCurvatureOn.setSelected(true);
        grainCurvatureOff.setSelected(false);

        vonNeumannRadio.setDisable(true);
        mooreRadio.setDisable(true);
        hexagonalRightRadio.setDisable(true);
        hexagonalLeftRadio.setDisable(true);
        hexagonalRandomRadio.setDisable(true);
        pentagonalRightRadio.setDisable(true);
        pentagonalLeftRadio.setDisable(true);
        pentagonalRandomRadio.setDisable(true);
        handleThresholdChange();
    }

    @FXML
    void handleGBCOff() {
        grainCurvature = false;
        deselectNeighbourhoods();
        grainCurvatureOff.setSelected(true);
        grainCurvatureOn.setSelected(false);
        neighbourhoodType = NeighbourhoodType.VonNeumann;
        vonNeumannRadio.setSelected(true);
        vonNeumannRadio.setDisable(false);
        mooreRadio.setDisable(false);
        hexagonalRightRadio.setDisable(false);
        hexagonalLeftRadio.setDisable(false);
        hexagonalRandomRadio.setDisable(false);
        pentagonalRightRadio.setDisable(false);
        pentagonalLeftRadio.setDisable(false);
        pentagonalRandomRadio.setDisable(false);
        handleThresholdChange();
    }

    @FXML
    public void handleThresholdChange() {
        try {
            String text = grainCurvatureRuleThreshold.getText();
            if (text.isEmpty()) {
                grainCurvatureRuleThreshold.setText("0.5");
                text = grainCurvatureRuleThreshold.getText();
            }
            this.grainThreshold = Double.parseDouble(text);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void deselectNeighbourhoods() {
        vonNeumannRadio.setSelected(false);
        mooreRadio.setSelected(false);
        hexagonalLeftRadio.setSelected(false);
        hexagonalRandomRadio.setSelected(false);
        hexagonalRightRadio.setSelected(false);
        pentagonalRandomRadio.setSelected(false);
        pentagonalLeftRadio.setSelected(false);
        pentagonalRightRadio.setSelected(false);
    }

    @FXML
    private void handleVonNeumann() {
        neighbourhoodType = NeighbourhoodType.VonNeumann;
        deselectNeighbourhoods();
        vonNeumannRadio.setSelected(true);

    }

    @FXML
    private void handleMoore() {
        neighbourhoodType = NeighbourhoodType.Moore;
        deselectNeighbourhoods();
        mooreRadio.setSelected(true);
    }

    @FXML
    private void handleHexagonalLeft() {
        neighbourhoodType = NeighbourhoodType.HexagonalLeft;
        deselectNeighbourhoods();
        hexagonalLeftRadio.setSelected(true);
    }

    @FXML
    private void handleHexagonalRight() {
        neighbourhoodType = NeighbourhoodType.HexagonalRight;
        deselectNeighbourhoods();
        hexagonalRightRadio.setSelected(true);
    }

    @FXML
    private void handleHexagonalRandom() {
        neighbourhoodType = NeighbourhoodType.HexagonalRandom;
        deselectNeighbourhoods();
        hexagonalRandomRadio.setSelected(true);
    }

    @FXML
    private void handlePentagonalRandom() {
        neighbourhoodType = NeighbourhoodType.PentagonalRandom;
        deselectNeighbourhoods();
        pentagonalRandomRadio.setSelected(true);
    }

    @FXML
    private void handlePentagonalRight() {
        neighbourhoodType = NeighbourhoodType.PentagonalRight;
        deselectNeighbourhoods();
        pentagonalRightRadio.setSelected(true);
    }

    @FXML
    private void handlePentagonalLeft() {
        neighbourhoodType = NeighbourhoodType.PentagonalLeft;
        deselectNeighbourhoods();
        pentagonalLeftRadio.setSelected(true);
    }

    @FXML
    private void handlePeriodic() {
        this.boundaryConditions = BoundaryConditions.Periodic;
        periodicRadio.setSelected(true);
        nonperiodicRadio.setSelected(false);
    }

    @FXML
    private void handleNonPeriodic() {
        this.boundaryConditions = BoundaryConditions.Nonperiodic;
        periodicRadio.setSelected(false);
        nonperiodicRadio.setSelected(true);
    }

    @FXML
    private void handleNextPhase() {

        for (Grain g : space.grains) {
            if (g.getMaterialType() == MaterialType.FERRITE)
                g.setMaterialType(MaterialType.AUSTENITE);
        }
        phaseVal++;
        phaseText.setText("Phase " + phaseVal);
    }

    @FXML
    private void handleDeleteGrain() {
        if (selectedGrain != null) {
            lockGUI();
            System.out.println("Grain: " + selectedGrain.toString());
            List<Field> toUpdate = selectedGrain.delete();

            for (Field f : toUpdate) {
                space.fields[f.getX()][f.getY()] = f;
            }

            System.out.println("Fields to update" + toUpdate.toString());
            this.space.updateImage(toUpdate);

            this.space.grains.remove(selectedGrain);
            updateOutput(space.returnImagePane(), space, true);
            unlockGUI();
        } else {
            System.out.println("Grain is null!");
        }

    }


    @FXML
    private void handleDestroyedMode() {

        if (destroyerMode.getValue()) {
            destroyerMode.setValue(true);
        } else {
            destroyerMode.setValue(false);
        }

    }

    @FXML
    private synchronized void handleGenerateStep() {
        lockGUI();
        stepsGenerationIndicator = Integer.parseInt(stepsVal.getText());

        try {
            GenerateStepTask generateStepTask = new GenerateStepTask((Space) space.clone(), this, stepsGenerationIndicator, this.neighbourhoodType);
            stepProgressBar.progressProperty().bind(generateStepTask.progressProperty());


            ExecutorService executorService
                    = Executors.newFixedThreadPool(1);
            executorService.execute(generateStepTask);
            executorService.shutdown();
            executorService.awaitTermination(60, TimeUnit.SECONDS);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @FXML
    private void handlePlayPauseButton() {

        if (play) {
            play = false;
            playPauseButton.setText("PLAY");
            unlockGUI();

        } else {
            lockGUI();
            play = true;
            playPauseButton.setText("PAUSE");
            recurrentGrow();

        }
    }

    @FXML
    public void recurrentGrow() {
        stepProgressBar.progressProperty().unbind();

        if (this.play && space.isPlace()) {

            try {

                GenerateRunTask generateRunTask = new GenerateRunTask((Space) space.clone(), this, this.neighbourhoodType);
                stepProgressBar.progressProperty().bind(generateRunTask.progressProperty());
                ExecutorService executorService
                        = Executors.newFixedThreadPool(1);
                executorService.execute(generateRunTask);
                executorService.shutdown();
                executorService.awaitTermination(60, TimeUnit.SECONDS);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (!space.isPlace()) {
            System.out.println("No more place!");
            playPauseButton.setText("PLAY");
            unlockGUI();
            play = false;
        } else if (!this.play) {
            playPauseButton.setText("PLAY");
            unlockGUI();
            play = false;
            updateOutput(space.returnImagePane(), space, true);
        } else {
            playPauseButton.setText("PLAY");
            unlockGUI();
            play = false;
            updateOutput(space.returnImagePane(), space, true);
        }
    }


    @FXML
    public void saveAsPng(ActionEvent event) {
        final FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Export As Image");


        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Image files (*.png)", "*.png");
        fileChooser.getExtensionFilters().add(extFilter);

        File file = fileChooser.showSaveDialog(((Node) event.getTarget()).getScene().getWindow());

        if (file != null) {
            WritableImage image = output.getChildren().get(output.getChildren().size() - 1).snapshot(new SnapshotParameters(), null);


            try {
                ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    @FXML
    public void dumpExport(ActionEvent event) {

        final FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Export as CSV");

        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("CSV files (*.csv)", "*.csv");
        fileChooser.getExtensionFilters().add(extFilter);

        File csvFile = fileChooser.showSaveDialog(((Node) event.getTarget()).getScene().getWindow());

        try {

            FileWriter writer = new FileWriter(csvFile);
            CSVUtils.writeLine(writer, Arrays.asList("x_size", "y_size", "background"));
            CSVUtils.writeLine(writer, Arrays.asList(Integer.toString(boardX), Integer.toString(boardY), "GREY"));
            CSVUtils.writeLine(writer, Arrays.asList("x", "y", "color", "phase", "isBoundary"));
            List<List<String>> dumpFields = space.dumpExport();
            for (int i = 0; i < dumpFields.size(); i++) {
                CSVUtils.writeLine(writer, dumpFields.get(i));
            }
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void dumpImport(ActionEvent event) {

        final FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Import from CSV");


        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("CSV files (*.csv)", "*.csv");
        fileChooser.getExtensionFilters().add(extFilter);

        File csvFile = fileChooser.showOpenDialog(((Node) event.getTarget()).getScene().getWindow());

        try {
            BufferedReader csvReader = new BufferedReader(new FileReader(csvFile));
            boolean continueFlag = true;
            String row = csvReader.readLine();
            String[] header = row.split(",");
            System.out.println(row);


            row = csvReader.readLine();
            String[] dimensions = row.split(",");
            System.out.println(row);

            boardX = Integer.parseInt(dimensions[0]);
            boardY = Integer.parseInt(dimensions[1]);
            space = new Space(boardX, boardY, this.boundaryConditions);

            space.fields = new Field[boardX][boardY];
            for (int i = 0; i < boardX; i++) {
                for (int j = 0; j < boardY; j++) {

                    space.fields[i][j] = new Field(i, j, null);
                }
            }
            row = csvReader.readLine();
            System.out.println(row);
            while (continueFlag) {
                row = csvReader.readLine();
                if (row != null) {
                    String[] data = row.split(",");

                    //TODO LAST IMPORT add to grains
                    int x = Integer.parseInt(data[0]);
                    int y = Integer.parseInt(data[1]);

                    int phase = Integer.parseInt(data[3]);
                    boolean isBoundary = Boolean.parseBoolean(data[4]);
                    space.fields[x][y].setBoundary(isBoundary);
                    Color color = Color.GRAY;
                    if (data[2].equals("0x808080ff")) {
                        color = Color.GRAY;
                    } else {
                        color = Color.web(data[2]);
                    }
                    boolean newGrain = true;
                    for (Grain grain : space.grains) {
                        if (grain.getColor().equals(color)) {
                            space.fields[x][y].setParent(grain);
                            grain.add(space.fields[x][y]);
                            newGrain = false;

                        } else {
                            newGrain = true;
                        }
                    }
                    if (newGrain) {
                        Grain g = new Grain(MaterialType.FERRITE, phase, color);
                        g.add(space.fields[x][y]);
                        space.fields[x][y].setParent(g);
                        space.grains.add(g);
                        space.addAlive(color);
                    }
                } else {
                    continueFlag = false;
                }
            }
            csvReader.close();
            output.setPrefSize(boardX, boardY);
            updateOutput(space.generate(), space, true);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public synchronized void updateOutput(AnchorPane image, Space space, boolean end) {
        if (space != null) {
            this.space = space;
        }
        if (image != null) {

            output.getChildren().clear();
            output.getChildren().add(image);
            stepProgressBar.progressProperty().unbind();
        } else {
            output.getChildren().clear();
            output.getChildren().add(space.returnImagePane());
            stepProgressBar.progressProperty().unbind();
        }
        if (end) {
            unlockGUI();
            playPauseButton.setText("PLAY");
            play = false;
        }
    }

    public int getPhase() {
        return phaseVal;
    }
}
